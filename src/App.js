import React, { Component } from 'react';
import './css/global.css';
import PageContent from './views/shared/PageContent';

class App extends Component {

  render() {
    return (
      <div className="App">
          <div id="page">
            <PageContent />
          </div>
      </div>
    );
  }
}

export default App;
