import React, { Component } from 'react';
import TopNav from '../shared/top-nav/TopNav';
import AdminSideNav from '../shared/AdminSideNav';
import AdminAgentsToolbar from './components/toolbar/AdminAgentsToolbar';
// import MyTable from './ReactTable';
import AdminAgentsTable from './components/table/AdminAgentsTable';

const AdminAgents = ()  => {
  return (
    <div>
      <TopNav />
      <AdminSideNav />
      <div className="page-content">
        <AdminAgentsToolbar />
        <AdminAgentsTable />
      </div>
    </div>
  );
}

export default AdminAgents;