import React from 'react';
import Modal from 'react-modal';
import FeatherIcon from 'feather-icons-react';

class ConfirmAutoAd extends React.Component {
    render() {
        return (
            <Modal
                isOpen={this.props.isOpen}
                className={this.props.className}
                onRequestClose={this.props.modalClosed}
                shouldCloseOnOverlayClick={true}
            >
                <div className="popup-header">
                    <h2>Add Agent</h2>
                    <div className="close-popup" onClick={this.props.modalClosed}>
                        <FeatherIcon icon={"x"} />
                    </div>
                </div>
                <p class="m-b-20">Are you sure you want to enable auto ads for Brian Clay? Enabling this will cause ads to run automatically with the following settings:</p>
                <div>
                    <div className="flex ai-start">
                        <div className="col">
                            <div className="template-detail flex ai-center black m-b-15" data-tippy-placement="top-start" data-tippy="Locations">
                                <FeatherIcon icon={"map"}/>
                                <span className="m-l-10">20mi Around Listing</span>
                            </div>
                            <div className="template-detail flex ai-center black m-b-15" data-tippy-placement="top-start" data-tippy="Gender">
                                <FeatherIcon icon={"users"}/>
                                <span className="m-l-10">Men &amp; Women</span>
                            </div>
                            <div className="template-detail flex ai-center black m-b-15" data-tippy-placement="top-start" data-tippy="Ages">
                                <FeatherIcon icon={"clock"}/>
                                <span className="m-l-10">20 - 40 years</span>
                            </div>
                        </div>
                        <div className="col">
                            <div className="template-detail flex ai-center black m-b-15" data-tippy-placement="top-start" data-tippy="Languages">
                                <FeatherIcon icon={"message-square"}/>
                                <span className="m-l-10">Any Language</span>
                            </div>
                            <div className="template-detail flex ai-center black m-b-15" data-tippy-placement="top-start" data-tippy="Duration">
                                <FeatherIcon icon={"calendar"}/>
                                <span className="m-l-10">3 Days</span>
                            </div>
                            <div className="template-detail flex ai-center black m-b-15" data-tippy-placement="top-start" data-tippy="Total Budget | Daily Budget">
                                <FeatherIcon icon={"dollar-sign"}/>
                                <span className="m-l-10">$100 | $33/day</span>
                            </div>
                        </div>
                    </div>
                    <div className="flex ai-start jc-between m-t-10 m-b-10">
                        <div className="help-label">
                            <label>Auto Apply Settings</label>
                            <img src="/assets/icons/info-blue.svg" data-tippy="Any listings matching the criteria will automatically be enrolled in this ad template." />
                        </div>
                        {/* <a href="#" class="inactive">Edit</a> */}
                    </div>
                    <div className="callout">
                        <div className="auto-apply-condition">
                            <div className="flex ai-center">
                                <div className="dropdown inactive" data-tippy-placement="bottom-end">
                                    <button className="dropdown-trigger">
                                        <span>Listing Status</span>
                                    </button>
                                    <ul className="dropdown-menu">
                                        <li className="dropdown-item flex ai-center">
                                            <label className="radio">One
                                            <input type="radio" defaultChecked="checked" name="radio" />
                                                <span className="radiobtn" />
                                            </label>
                                            <span className="item-label">Daily Budget</span>
                                        </li>
                                        <li className="dropdown-item flex ai-center">
                                            <label className="radio">
                                                <input type="radio" />
                                                <span className="radiobtn" />
                                            </label>
                                            <span className="item-label">Total Budget</span>
                                        </li>
                                    </ul>
                                </div>
                                <div className="dropdown inactive m-l-10" data-tippy-placement="bottom-end">
                                    <button className="dropdown-trigger">
                                        <span>Equals</span>
                                    </button>
                                    <ul className="dropdown-menu">
                                        <li className="dropdown-item flex ai-center">
                                            <label className="radio">One
                                            <input type="radio" defaultChecked="checked" name="radio" />
                                                <span className="radiobtn" />
                                            </label>
                                            <span className="item-label">Daily Budget</span>
                                        </li>
                                        <li className="dropdown-item flex ai-center">
                                            <label className="radio">
                                                <input type="radio" />
                                                <span className="radiobtn" />
                                            </label>
                                            <span className="item-label">Total Budget</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div className="dropdown inactive m-t-10" data-tippy-placement="bottom-end">
                                <button className="dropdown-trigger w-100 jc-between">
                                    <span>New</span>
                                </button>
                                <ul className="dropdown-menu">
                                    <li className="dropdown-item flex ai-center">
                                        <label className="radio">One
                                        <input type="radio" defaultChecked="checked" name="radio" />
                                            <span className="radiobtn" />
                                        </label>
                                        <span className="item-label">Daily Budget</span>
                                    </li>
                                    <li className="dropdown-item flex ai-center">
                                        <label className="radio">
                                            <input type="radio" />
                                            <span className="radiobtn" />
                                        </label>
                                        <span className="item-label">Total Budget</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div className="popup-buttons">
                        <button className="close-popup btn-cancel" onClick={this.props.modalClosed}>Cancel</button>
                        <button className="close-popup btn-primary" onClick={this.props.modalClosed}>Confirm</button>
                    </div>
                </div>

            </Modal>
        );
    }
}

export default ConfirmAutoAd;