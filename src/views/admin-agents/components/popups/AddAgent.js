import React from 'react';
import Modal from 'react-modal';
import FeatherIcon from 'feather-icons-react';

class AddAgentPopup extends React.Component {

    render() {
        return (
            <Modal
                isOpen={this.props.isOpen}
                className={this.props.className}
                onRequestClose={this.props.modalClosed}
                shouldCloseOnOverlayClick={true}
            >
                <div className="popup-header">
                    <h2>Add Agent</h2>
                    <div className="close-popup" onClick={this.props.modalClosed}>
                        <FeatherIcon icon={"x"} />
                    </div>
                </div>
                <label>Brokerage</label>
                <div className="dropdown">
                    <button className="dropdown-trigger w-100 jc-between">Broker 1</button>
                    <ul className="dropdown-menu">
                        <li className="dropdown-item">
                            <span className="dropdown-label">Broker 1</span>
                        </li>
                        <li className="dropdown-item">
                            <span className="dropdown-label">Broker 2</span>
                        </li>
                        <li className="dropdown-item">
                            <span className="dropdown-label">Broker 3</span>
                        </li>
                    </ul>
                </div>
                <div className="input-wrapper w-100 m-t-20">
                    <label>Agent Name</label>
                    <input placeholder="Enter Agent Name" />
                    <div className="input-message">
                        <img src="/assets/icons/check-circle-green.svg" />
                        <span>This is a message</span>
                    </div>
                </div>
                <div className="popup-buttons">
                    <button className="close-popup btn-cancel" onClick={this.props.modalClosed}>Cancel</button>
                    <button className="close-popup btn-primary" oonClick={this.props.modalClosed}>Add Agent</button>
                </div>
            </Modal>
        );
    }
}

export default AddAgentPopup;