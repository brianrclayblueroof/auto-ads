import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Tippy from '@tippy.js/react';
import 'tippy.js/dist/tippy.css';
import userprofiledata from '../../../shared/data/userprofiledata';
import FeatherIcon from 'feather-icons-react';
import AssignedToDropdown from './AssignedToDropdown';
import ConfirmAutoAd from '../popups/ConfirmAutoAd';

class AdminAgentsTable extends Component {
  constructor(props){
    super(props)
    this.state = {
      confirmAutoAdOpen: false
    }
    this.openModal = this.openModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
  }

  openModal() {
    this.setState({confirmAutoAdOpen: true});
  }

  closeModal() {
    this.setState({confirmAutoAdOpen: false});
  }

  render() {
    return (
      <div>

        <div className="panel hide">
          <div className="callout primary">
            You haven’t added any agents yet. Click add agent above to get started.
          </div>
        </div>

        <div className="panel w-100">
          <table id="my-agents-table">
            <thead>
              <tr>
                <th className="my-agents-table-name">
                  <Tippy content={<span>Select All</span>}>
                    <label className="checkbox" data-tippy="Select All">
                      <input type="checkbox" />
                      <span className="checkmark" />
                    </label>
                  </Tippy>
                  <span>Name</span>
                  <div className="sort" />
                </th>
                <th className="my-agents-table-brokerage">
                  <span>Brokerage</span>
                  <div className="sort" />
                </th>
                <th className="my-agents-table-use-auto-ad ta-center" data-tippy="Enable to Run Default Ad Template" data-sortable="false">
                  <span>Use Auto Ad</span>
                  <div className="sort" />
                </th>
                <th className="my-agents-table-total-listings">
                  <span># of Listings</span>
                  <div className="sort" />
                </th>
                <th className="my-agents-table-total-ads">
                  <span># of Current Ads</span>
                  <div className="sort" />
                </th>
                <th className="my-agents-table-ave-cpc">
                  <span>Ave CPC</span>
                  <div className="sort" />
                </th>
                <th className="my-agents-table-ave-clicks">
                  <span>Ave # of Clicks</span>
                  <div className="sort" />
                </th>
                <th className="my-agents-table-assigned-to" data-selectable="false">
                  <span>Assigned To</span>
                  <div className="sort" />
                </th>
                <th className="ta-right my-agents-table-login" data-sortable="false" />
                <th className="my-agents-table-delete" data-sortable="false" />
              </tr>
            </thead>
            <tbody>
              <tr>
                <td className="my-agents-table-name">
                  <div className="flex ai-center">
                    <label className="checkbox">
                      <input type="checkbox" />
                      <span className="checkmark" />
                    </label>
                    <div className="table-avatar m-r-15" style ={ { backgroundImage: "url("+require('../../../../assets/images/placeholder/profile-img.png')+")" } } />
                    <span>Jason Clay</span>
                  </div>
                </td>
                <td className="my-agents-table-brokerage">
                  <span>Aroperty One</span>
                </td>
                <td className="my-agents-table-use-auto-ad ta-center">
                  <label className="checkbox" onClick={this.openModal}>
                    <input type="checkbox" />
                    <span className="checkmark" />
                  </label>
                </td>
                <td className="my-agents-table-total-listings">
                  10
                </td>
                <td className="my-agents-table-total-ads">
                  <span>20</span>
                </td>
                <td className="my-agents-table-ave-cpc">
                  <span>$3.14</span>
                </td>
                <td className="my-agents-table-ave-clicks">
                  <span>200</span>
                </td>
                <td className="my-agents-table-assigned-to">
                  <AssignedToDropdown />
                </td>
                <td className="ta-right my-agents-table-login">
                  <Link to="/listings">
                    <div href="/listings" className="login-as-agent btn">Login</div>
                  </Link>
                </td>
                <td className="my-agents-table-delete actions grey">
                  <Tippy content={<span>Remove Agent</span>}>
                    <FeatherIcon icon={'trash-2'}/>
                  </Tippy>
                </td>
              </tr>
              <tr className="my-agents-table-row">
                <td className="my-agents-table-name">
                  <div className="flex ai-center">
                    <label className="checkbox">
                      <input type="checkbox" />
                      <span className="checkmark" />
                    </label>
                    <div className="table-avatar m-r-15" style ={ { backgroundImage: "url("+require('../../../../assets/images/placeholder/profile-img.png')+")" } } />
                    <span>Brian Clay</span>
                  </div>
                </td>
                <td className="my-agents-table-brokerage">
                  <span>Property One</span>
                </td>
                <td className="my-agents-table-use-auto-ad ta-center">
                  <label className="checkbox" onClick={this.openModal}>
                    <input type="checkbox" />
                    <span className="checkmark" />
                  </label>
                </td>
                <td className="my-agents-table-total-listings">
                  12
                </td>
                <td className="my-agents-table-total-ads">
                  <span>20</span>
                </td>
                <td className="my-agents-table-ave-cpc">
                  <span>$1.12</span>
                </td>
                <td className="my-agents-table-ave-clicks">
                  <span>201</span>
                </td>
                <td className="my-agents-table-assigned-to">
                  <AssignedToDropdown />
                </td>
                <td className="ta-right my-agents-table-login">
                  <Link to="/listings">
                    <div href="/listings" className="login-as-agent btn">Login</div>
                  </Link>
                </td>
                <td className="my-agents-table-delete actions grey">
                  <Tippy content={<span>Remove Agent</span>}>
                    <FeatherIcon icon={'trash-2'}/>
                  </Tippy>
                </td>
              </tr>
              <tr className="my-agents-table-row">
                <td className="my-agents-table-name">
                  <div className="flex ai-center">
                    <label className="checkbox">
                      <input type="checkbox" />
                      <span className="checkmark" />
                    </label>
                    <div className="table-avatar m-r-15" style ={ { backgroundImage: "url("+require('../../../../assets/images/placeholder/profile-img.png')+")" } } />
                    <span>David Clay</span>
                  </div>
                </td>
                <td className="my-agents-table-brokerage">
                  <span>Property One</span>
                </td>
                <td className="my-agents-table-use-auto-ad ta-center">
                  <label className="checkbox" onClick={this.openModal}>
                    <input type="checkbox" />
                    <span className="checkmark" />
                  </label>
                </td>
                <td className="my-agents-table-total-listings">
                  11
                </td>
                <td className="my-agents-table-total-ads">
                  <span>20</span>
                </td>
                <td className="my-agents-table-ave-cpc">
                  <span>$3.15</span>
                </td>
                <td className="my-agents-table-ave-clicks">
                  <span>210</span>
                </td>
                <td className="my-agents-table-assigned-to">
                  <AssignedToDropdown />
                </td>
                <td className="ta-right my-agents-table-login">
                  <Link to="/listings">
                    <div href="/listings" className="login-as-agent btn">Login</div>
                  </Link>
                </td>
                <td className="my-agents-table-delete actions grey">
                  <Tippy content={<span>Remove Agent</span>}>
                    <FeatherIcon icon={'trash-2'}/>
                  </Tippy>
                </td>
              </tr>
            </tbody>
          </table>
        </div>

        <ConfirmAutoAd 
          isOpen = {this.state.confirmAutoAdOpen}
          modalClosed = {this.closeModal}
        />
      </div>


    );
  }
}

export default AdminAgentsTable;