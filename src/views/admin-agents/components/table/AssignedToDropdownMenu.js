import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import FeatherIcon from 'feather-icons-react';

const adminUsers = [
    {
        name: "Ben Kinney",
        profilePic: null,
    },
    {
        name: "Jarad Hull",
        profilePic: "https://vignette.wikia.nocookie.net/darth/images/7/7e/Wat-Meme-Old-Lady-01.jpg/revision/latest?cb=20160415121355",
    }
]

class AssignedToDropdownMenu extends Component {
    constructor(props) {
        super(props)
        this.state = {
            adminUserEnabled: true
        }
    }

    toggleAdminUser(e) {
        console.log('hi')
    }

    render() {
        return (
            <ul className="dropdown-menu checkbox-menu">
                {adminUsers.map(adminUser => (
                    <li key={{adminUser}} className="dropdown-item checkbox-item" onClick={()=>{this.toggleAdminUser()}}>
                        <label className="checkbox">
                            <input type="checkbox" defaultChecked={this.state.adminUserEnabled} />
                            <span className="checkmark" />
                        </label>
                        { adminUser.profilePic ? 
                            <div className="table-avatar m-r-10 m-l-5" 
                            style ={ { backgroundImage: "url("+ adminUser.profilePic +")" } } /> 
                            :
                            <div className="table-avatar m-r-10 m-l-5" 
                            style ={ { backgroundImage: "url("+require('../../../../assets/images/placeholder/profile-img.png')+")" } } />
                        }

                        <span className="dropdown-label">{adminUser.name}</span>
                    </li>
                ))}
            </ul>
        );
    }
}

export default AssignedToDropdownMenu;