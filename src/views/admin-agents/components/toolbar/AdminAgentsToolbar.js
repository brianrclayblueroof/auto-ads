import React, { Component } from 'react';
import SearchBar from '../../../shared/SearchBar';
import BrokerFilter from './BrokerFilter';
import AssignedToFilter from '../../../shared/assigned-to-filter/AssignedToFilter';
import AddAgentPopup from '../popups/AddAgent';
import Button from '../../../shared/Button';

class AdminAgentsToolbar extends Component {
  constructor(props){
    super(props)
    this.state = {
      addAgentOpen: false
    }
    this.openModal = this.openModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
  }

  openModal() {
    this.setState({addAgentOpen: true});
  }

  closeModal() {
    this.setState({addAgentOpen: false});
  }

  render() {
    return (
      <div className="panel flex jc-between ai-center tool-bar m-b-20">
        <div className="tool-bar-left flex ai-center flex1">
          <SearchBar />
          <BrokerFilter />
          <AssignedToFilter />
        </div>
        <div className="tool-bar-right">
          <Button 
            className={"btn-primary"}
            buttonText={"Add Agent"}
            onClick={this.openModal}/>
        </div>
        <AddAgentPopup 
          isOpen = {this.state.addAgentOpen}
          modalClosed = {this.closeModal}/>
      </div>

    );
  }
}

export default AdminAgentsToolbar;