import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import FeatherIcon from 'feather-icons-react';

const brokers = [
    "Broker 1",
    "Broker 2"
]

class ProfileDropdownMenu extends Component {
    constructor(props) {
        super(props)
        this.state = {
            brokerEnabled: true
        }
    }

    toggleBroker(e) {
        console.log('hi')
    }

    render() {
        return (
            <ul className="dropdown-menu checkbox-menu">
                {brokers.map(broker => (
                    <li key={{broker}} className="dropdown-item checkbox-item" onClick={()=>{this.toggleBroker()}}>
                        <label className="checkbox">
                            <input type="checkbox" defaultChecked={this.state.brokerEnabled} />
                            <span className="checkmark" />
                        </label>
                        <span className="dropdown-label">{broker}</span>
                    </li>
                ))}
            </ul>
        );
    }
}

export default ProfileDropdownMenu;