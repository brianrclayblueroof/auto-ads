import React, { Component } from 'react';
import FeatherIcon from 'feather-icons-react';
import Tippy from '@tippy.js/react';

class TemplateLoactions extends Component {
    render() {
        return (
            <div className="panel">
                <div className="flex ai-start jc-between">
                    <div className="help-label">
                        <label>Locations</label>
                        <span class="blue m-l-5">
                            <Tippy content="Ads using this template will reach people in these locations.">
                                <FeatherIcon icon={"info"} width={16}/>
                            </Tippy>
                        </span>
                    </div>
                    <a href="JavaScript:void(0)" className="inactive">Edit</a>
                </div>
                <div className="flex ai-center black m-t-10">
                    <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M10.0003 0C5.41699 0 1.66699 3.75 1.66699 8.33333C1.66699 14.5 9.25033 19.6667 9.50033 19.8333C9.66699 19.9167 9.83366 20 10.0003 20C10.167 20 10.3337 19.9167 10.5003 19.8333C10.7503 19.6667 18.3337 14.5 18.3337 8.33333C18.3337 3.75 14.5837 0 10.0003 0Z" fill="#13CE66" />
                        <mask id="mask0" mask-type="alpha" maskUnits="userSpaceOnUse" x="5" y="3" width="10" height="10">
                            <path fill-rule="evenodd" clip-rule="evenodd" d="M13.9997 6.4235L10.2497 3.51015C10.083 3.38529 9.87467 3.38529 9.74967 3.51015L5.99967 6.4235C5.87467 6.50674 5.83301 6.6316 5.83301 6.75646V12.5832H7.08301H12.9163H14.1663V6.75646C14.1663 6.6316 14.1247 6.50674 13.9997 6.4235ZM8.49955 12.5818H11.4996V7.99976H8.49955V12.5818Z" fill="white" />
                        </mask>
                        <g mask="url(#mask0)">
                            <path fill-rule="evenodd" clip-rule="evenodd" d="M5 3H15V13H5V3Z" fill="white" />
                        </g>
                    </svg>
                    <span className="m-l-10">20mi Radius Around Listings</span>
                </div>
            </div>

        );
    }
}

export default TemplateLoactions;