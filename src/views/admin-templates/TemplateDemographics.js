import React, { Component } from 'react';
import FeatherIcon from 'feather-icons-react';
import Tippy from '@tippy.js/react';

class TemplateDemographics extends Component {
    render() {
        return (
            <div className="panel">
                <div className="flex ai-start jc-between">
                    <div className="help-label">
                        <label>Demographics</label>
                        <span class="blue m-l-5">
                            <Tippy content="Ads using this template will reach people with these demographics.">
                                <FeatherIcon icon={"info"} width={16}/>
                            </Tippy>
                        </span>
                    </div>
                    <a href="JavaScript:void(0)" className="inactive">Edit</a>
                </div>
                <div className="flex ai-start m-t-10 flex-wrap">
                    <div className="col">
                        <Tippy placement={"top-start"} content="Gender">
                            <div className="flex ai-center black m-b-15">
                                <FeatherIcon icon={'users'} width={18} />
                                <span className="m-l-10">Men &amp; Women</span>
                            </div>
                        </Tippy>
                        <Tippy placement={"top-start"} content="Ages">
                            <div className="flex ai-center black">
                                <FeatherIcon icon={'clock'} width={18} />
                                <span className="m-l-10">20 - 40 Years</span>
                            </div>
                        </Tippy>
                    </div>
                    <div className="col">
                        <Tippy placement={"top-start"} content="Languages">
                            <div className="flex ai-center black">
                                <FeatherIcon icon={'message-square'} width={18} />
                                <span className="m-l-10">Any Language</span>
                            </div>
                        </Tippy>
                    </div>
                </div>
            </div>

        );
    }
}

export default TemplateDemographics;