import React, { Component } from 'react';
import FeatherIcon from 'feather-icons-react';
import Tippy from '@tippy.js/react';

class TemplateAutoApply extends Component {
    render() {
        return (
            <div className="panel">
                <div className="flex ai-start jc-between">
                    <div className="help-label">
                        <label>Auto Apply Settings</label>
                        <span class="blue m-l-5">
                            <Tippy content="Any listings matching the criteria will automatically be enrolled in this ad template." >
                                <FeatherIcon icon={"info"} width={16}/>
                            </Tippy>
                        </span>
                    </div>
                    <a href="JavaScript:void(0)" className="inactive">Edit</a>
                </div>
                <div className="callout m-t-10">
                    <div className="auto-apply-condition">
                        <div className="flex ai-center">
                            <div className="dropdown inactive" data-tippy-placement="bottom-end">
                                <button className="dropdown-trigger">
                                    <span>Listing Status</span>
                                </button>
                                <ul className="dropdown-menu">
                                    <li className="dropdown-item flex ai-center">
                                        <label className="radio">One
                                            <input type="radio" defaultChecked="checked" name="radio" />
                                            <span className="radiobtn" />
                                        </label>
                                        <span className="item-label">Daily Budget</span>
                                    </li>
                                    <li className="dropdown-item flex ai-center">
                                        <label className="radio">
                                            <input type="radio" />
                                            <span className="radiobtn" />
                                        </label>
                                        <span className="item-label">Total Budget</span>
                                    </li>
                                </ul>
                            </div>
                            <div className="dropdown inactive m-l-10" data-tippy-placement="bottom-end">
                                <button className="dropdown-trigger">
                                    <span>Equals</span>
                                </button>
                                <ul className="dropdown-menu">
                                    <li className="dropdown-item flex ai-center">
                                        <label className="radio">One
                                            <input type="radio" defaultChecked="checked" name="radio" />
                                            <span className="radiobtn" />
                                        </label>
                                        <span className="item-label">Daily Budget</span>
                                    </li>
                                    <li className="dropdown-item flex ai-center">
                                        <label className="radio">
                                            <input type="radio" />
                                            <span className="radiobtn" />
                                        </label>
                                        <span className="item-label">Total Budget</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div className="dropdown inactive m-t-10" data-tippy-placement="bottom-end">
                            <button className="dropdown-trigger w-100 jc-between">
                                <span>New</span>
                            </button>
                            <ul className="dropdown-menu">
                                <li className="dropdown-item flex ai-center">
                                    <label className="radio">One
                                        <input type="radio" defaultChecked="checked" name="radio" />
                                        <span className="radiobtn" />
                                    </label>
                                    <span className="item-label">Daily Budget</span>
                                </li>
                                <li className="dropdown-item flex ai-center">
                                    <label className="radio">
                                        <input type="radio" />
                                        <span className="radiobtn" />
                                    </label>
                                    <span className="item-label">Total Budget</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

        );
    }
}

export default TemplateAutoApply;