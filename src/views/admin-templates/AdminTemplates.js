import React, { Component } from 'react';
import TopNav from '../shared/top-nav/TopNav';
import AdminSideNav from '../shared/AdminSideNav';
import TemplateName from './TemplateName';
import TemplateDemographics from './TemplateDemographics';
import TemplateLocations from './TemplateLocations';
import TemplateBudget from './TemplateBudget';
import TemplateAutoApply from './TemplateAutoApply';
import TemplateAdPreview from './ad-previews/TemplateAdPreivew';

class AdminTemplates extends Component {
  render() {
    return (
      <div>
        <TopNav />
        <AdminSideNav />
        <div className="page-content flex ai-start constrain">
          <div className="col flex flex-wrap ai-start template-left-content">
            <div className="col">
              <TemplateName />
              <TemplateDemographics />
              <TemplateLocations />              
            </div>
            <div className="col">
              <TemplateBudget />
              <TemplateAutoApply />
            </div>
          </div>
          <div className="col">
            <TemplateAdPreview />
          </div>
        </div>
      </div>
    );
  }
}

export default AdminTemplates;