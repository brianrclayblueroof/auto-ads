import React, { Component } from 'react';
import FeatherIcon from 'feather-icons-react';

class TemplateAdPreview extends Component {
    render() {
        return (
            <div className="panel">
                <label>Ad Preview</label>
                <div className="fb-ad-preview single-img">
                    <div className="preview-top flex ai-start">
                        <div className="preview-avatar" style ={ { backgroundImage: "url("+require('../../../assets/images/placeholder/profile-img.png')+")" } } />
                        <div className="profile-info">
                            <p className="profile-name facebook">Primary FB Profile Name</p>
                            <div className="sponsored flex ai-center">
                                <p className="grey m-r-10">Sponsored</p>
                                <div className="sponsored-dot" />
                                <img alt="" className="public-icon" src={require("../../../assets/icons/fb-public.png")} />
                            </div>
                        </div>
                    </div>
                    <p className="preview-text">
                        Beautiful new listing at [LISTING ADDRESS]. You do not want to miss out on this one! Contact me today for more info! [AGENT PHONE]
                    </p>
                    <div className="preview-img grey">
                        <FeatherIcon icon={'home'} />
                        <p className="preview-img-text grey">First image of first listing</p>
                    </div>
                    <div className="preview-bottom">
                        <div className="preview-cta-info">
                            <p className="preview-URL">Agents Homepage URL</p>
                            <p className="preview-headline">[LISTING ADDRESS]</p>
                            <p className="preview-description">[LISTING PRICE] | [LISTING BEDS] beds | [LISTING BATHS] baths</p>
                        </div>
                        <button className="preview-cta learn-more">Learn More</button>
                    </div>
                    <div className="share-btns flex ai-center jc-between p-10 m-t-10">
                        <img alt="" src={require("../../../assets/icons/fb-like.png")} />
                        <img alt="" src={require("../../../assets/icons/fb-comment.png")} />
                        <img alt="" src={require("../../../assets/icons/fb-share.png")} />
                    </div>
                </div>
                <div className="preview-ations flex ai-center">
                    <div className="edit-content inactive pointer flex ai-center w-100 jc-center">
                        <FeatherIcon icon={'edit-3'} width={20} />
                        <span className="m-l-10">Edit Content</span>
                    </div>
                    <div className="refresh-preview inactive pointer flex ai-center w-100 jc-center">
                        <FeatherIcon icon={'rotate-ccw'} width={20} />
                        <span className="m-l-10">Refresh Preview</span>
                    </div>
                </div>
            </div>

        );
    }
}

export default TemplateAdPreview;