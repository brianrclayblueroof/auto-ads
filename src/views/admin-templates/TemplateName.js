import React, { Component } from 'react';

class TemplateName extends Component {
    render() {
        return (
            <div className="panel">
                <div className="input-wrapper w-100">
                    <label>Template Name</label>
                    <input placeholder="Default Template" className="w-100" />
                </div>
            </div>
        );
    }
}

export default TemplateName;