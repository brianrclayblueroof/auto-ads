import React, { Component } from 'react';
import FeatherIcon from 'feather-icons-react';
import Tippy from '@tippy.js/react';

class TempalteBudget extends Component {
    render() {
        return (
            <div className="panel">
                <div className="flex ai-start jc-between">
                    <div className="help-label">
                        <label>Budget</label>
                        <span class="blue m-l-5">
                            <Tippy content="Ads using this template will use these settings for their budget.">
                                <FeatherIcon icon={"info"} width={16}/>
                            </Tippy>
                        </span>
                    </div>
                    <a href="JavaScript:void(0)" className="inactive">Edit</a>
                </div>
                <div className="inline-flex ai-center">
                    <div className="dropdown inactive" data-tippy-placement="bottom-end">
                        <button className="dropdown-trigger">
                            <span>Daily Budget</span>
                        </button>
                        <ul className="dropdown-menu">
                            <li className="dropdown-item flex ai-center">
                                <label className="radio">One
                                <input type="radio" defaultChecked="checked" name="radio" />
                                    <span className="radiobtn" />
                                </label>
                                <span className="item-label">Daily Budget</span>
                            </li>
                            <li className="dropdown-item flex ai-center">
                                <label className="radio">
                                    <input type="radio" />
                                    <span className="radiobtn" />
                                </label>
                                <span className="item-label">Total Budget</span>
                            </li>
                        </ul>
                    </div>
                    <div className="input-wrapper w-100 flex1 m-l-5">
                        <input placeholder="Daily Budget" defaultValue="$10.00" className="outline-input inactive" />
                        <div className="input-message">
                            <img alt="" src="/assets/icons/alert-triangle-red.svg" />
                            <span>This is an error message</span>
                        </div>
                    </div>
                </div>
                <div className="help-label m-t-30">
                    <label>Duration</label>
                    <img alt="" src="/assets/icons/info-blue.svg" data-tippy="Ads using this template will run for the amount of time shown below." />
                </div>
                <div className="inline-flex ai-center">
                    <div className="input-wrapper w-100 flex1 m-r-5">
                        <input defaultValue={3} className="outline-input inactive" />
                        <div className="input-message">
                            <img alt="" src="/assets/icons/alert-triangle-red.svg" />
                            <span>This is an error message</span>
                        </div>
                    </div>
                    <div className="dropdown inactive" data-tippy-placement="bottom-end">
                        <button className="dropdown-trigger">
                            <span>Days</span>
                        </button>
                        <ul className="dropdown-menu">
                            <li className="dropdown-item flex ai-center">
                                <label className="radio">One
                                <input type="radio" defaultChecked="checked" name="radio" />
                                    <span className="radiobtn" />
                                </label>
                                <span className="item-label">Days</span>
                            </li>
                            <li className="dropdown-item flex ai-center">
                                <label className="radio">
                                    <input type="radio" />
                                    <span className="radiobtn" />
                                </label>
                                <span className="item-label">Weeks</span>
                            </li>
                            <li className="dropdown-item flex ai-center">
                                <label className="radio">
                                    <input type="radio" />
                                    <span className="radiobtn" />
                                </label>
                                <span className="item-label">Months</span>
                            </li>
                        </ul>
                    </div>
                </div>
                <div className="callout primary m-t-30">
                    Ads using this template will run for <strong>3</strong> days. You’ll spend no more than
                    <strong>$100.83</strong>.
                </div>
            </div>

        );
    }
}

export default TempalteBudget;