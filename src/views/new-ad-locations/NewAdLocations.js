import React, { Component } from 'react';
import TopNav from '../shared/top-nav/TopNav';
import SideNav from '../shared/SideNav';
import NewAdLocationsToolbar from './components/NewAdLocationsToolbar';
import NewAdLocationsContent from './components/NewAdLocationsContent';

class NewAdLocations extends Component {
    render() {
        return (
            <div>
                <TopNav />
                <SideNav />
                <div className="page-content">
                    <NewAdLocationsToolbar />
                    <NewAdLocationsContent />
                </div>
            </div>
        );
    }
}

export default NewAdLocations;