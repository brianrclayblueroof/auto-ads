import React, { Component } from 'react';

class AreaRadiusSelectMenu extends Component {

    render() {
        return (
            <div className="dropdown-menu">
                <div className="dropdown-content p-20">
                    <div className="help-label m-b-10">
                        <label>Radius Around Listings</label>
                    </div>
                    <div className="inline-flex ai-center">
                        <div className="input-wrapper w-100 flex1">
                            <input className="outline-input" defaultValue={20} min={1} type="number" />
                        </div>
                        <button className="dropdown-trigger">
                            <span>Miles</span>
                        </button>
                    </div>
                    <div className="flex ai-center m-t-20" data-tippy="Constrain to this area instead of a radius">
                        <label className="checkbox">
                            <input type="checkbox" />
                            <span className="checkmark" />
                        </label>
                        <span className="status-label">This Area Only</span>
                    </div>
                </div>
            </div>
        );
    }
}

export default AreaRadiusSelectMenu;