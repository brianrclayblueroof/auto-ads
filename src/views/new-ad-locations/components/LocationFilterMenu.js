import React, { Component } from 'react';

class IncludeExcludeMenu extends Component {

    render() {
        return (
            <ul className="dropdown-menu">
                <li className="dropdown-item">
                    <label className="radio">
                        Everyone in this location
                        <input type="radio" name="location-filter" defaultChecked="true" />
                        <span className="radio-indicator" />
                    </label>
                </li>
                <li className="dropdown-item">
                    <label className="radio">
                        People who live in this location
                        <input type="radio" name="location-filter" />
                        <span className="radio-indicator" />
                    </label>
                </li>
                <li className="dropdown-item">
                    <label className="radio">
                        People recently in this location
                        <input type="radio" name="location-filter" />
                        <span className="radio-indicator" />
                    </label>
                </li>
                <li className="dropdown-item">
                    <label className="radio">
                        People traveling in this location
                        <input type="radio" name="location-filter" />
                        <span className="radio-indicator" />
                    </label>
                </li>
            </ul>
        );
    }
}

export default IncludeExcludeMenu;