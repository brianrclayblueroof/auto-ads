import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Button from '../../shared/Button';
import FeatherIcon from 'feather-icons-react';
import Tippy from '@tippy.js/react';

class NewAdLocationsToolbar extends Component {
    render() {
        return (
            <div className="panel flex jc-between ai-center tool-bar m-b-20">
                <div className="tool-bar-left flex ai-center flex1">
                    <Tippy content="Previous Step">
                        <Link to="/new-ad">
                            <div href="/new-ad-type" className="step-back">
                                <FeatherIcon icon="chevron-left"/>
                            </div>
                        </Link>
                    </Tippy>
                    <h2>Locations</h2>
                </div>
                <div className="tool-bar-right">
                    <div className="btn-group">
                        <Link to="/my-ads">
                            <Button 
                                className="btn-cancel"
                                buttonText="Cancel"
                            />
                        </Link>
                        <Link to="/my-ads">
                            <Button 
                                className="btn-inactive"
                                buttonText="Save Draft"
                            />
                        </Link>
                        <Link to="/new-ad-demographics">
                            <Button 
                                className="btn-primary"
                                buttonText="Continue"
                            />
                        </Link>
                    </div>
                </div>
            </div>


        );
    }
}

export default NewAdLocationsToolbar;