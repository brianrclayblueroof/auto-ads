import React, { Component } from 'react';

class IncludeExcludeMenu extends Component {

    render() {
        return (
            <ul className="dropdown-menu">
                <li className="dropdown-item">
                    <label className="radio">
                        Include
                        <input type="radio" name="include-exclude" defaultChecked="true" />
                        <span className="radio-indicator" />
                    </label>
                </li>
                <li className="dropdown-item">
                    <label className="radio inactive">
                        Exclude
                        <input type="radio" name="include-exclude" />
                        <span className="radio-indicator" />
                    </label>
                </li>
            </ul>
        );
    }
}

export default IncludeExcludeMenu;