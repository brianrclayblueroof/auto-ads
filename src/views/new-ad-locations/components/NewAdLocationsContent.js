import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Button from '../../shared/Button';
import FeatherIcon from 'feather-icons-react';
import Tippy from '@tippy.js/react';
import SearchBar from '../../shared/SearchBar';
import IncludeExclude from './IncludeExclude';
import LocationFilter from './LocationsFilter';
import AreaRadiusSelect from './AreaRadiusSelect';

class NewAdLocationsContent extends Component {
    render() {
        return (
            <div className="panel m-b-40">
                <div className="flex ai-center">
                    <SearchBar 
                        placeholder="Start typing to add a location"
                    />
                    <IncludeExclude />
                    <LocationFilter />
                </div>
                <div className="map-wrapper m-t-20">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d48689.8856316789!2d-111.7304819758714!3d40.295342136717714!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x874d7804e4b45119%3A0x642bc3cf90b21ed1!2sOrem%2C+UT!5e0!3m2!1sen!2sus!4v1544208702073" width="100%" height={450} frameBorder={0} style={{ border: 0 }} allowFullScreen />
                </div>
                <div className="locations-list">
                    <div className="loaction p-t-20 p-b-20 flex ai-center jc-between">
                        <div className="flex ai-center">
                            <Tippy content="Exclude Area">
                                <svg width={20} height={20} viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M5.22271 5.22271L17.7785 2.22266V17.7785H2.22266L5.22271 5.22271Z" fill="#FF5216" fillOpacity="0.376" />
                                    <path d="M6.29204 5.92193L5.9933 5.99331L5.92193 6.29204L3.54648 16.2337L3.39924 16.8499H4.03279H16.3499H16.8499V16.3499V4.03279V3.39924L16.2337 3.54648L6.29204 5.92193ZM5.63765 5.63765L17.2785 2.8562V17.2785H2.8562L5.63765 5.63765Z" fill="#FF5216" stroke="#FF5216" />
                                    <ellipse cx="17.7779" cy="2.22226" rx="2.22226" ry="2.22226" fill="#FF5216" />
                                    <ellipse cx="5.22226" cy="5.22226" rx="2.22226" ry="2.22226" fill="#FF5216" />
                                    <ellipse cx="2.22226" cy="17.7779" rx="2.22226" ry="2.22227" fill="#FF5216" />
                                    <ellipse cx="17.7779" cy="17.7779" rx="2.22226" ry="2.22227" fill="#FF5216" />
                                </svg>
                            </Tippy>
                            <span className="loaction-name m-l-10">West Jordan, UT</span>
                        </div>
                        <div className="location-actions flex ai-center">
                            <IncludeExclude />
                            <LocationFilter />
                            <AreaRadiusSelect />
                        </div>
                    </div>
                </div>
                <div className="help-label m-t-20 m-b-10">
                    <label>Include Area Around Listings</label>
                    <Tippy content="You can choose to inlude the area around all listings associated with this ad">
                        <FeatherIcon icon="info" className="blue m-l-5" width={16} />
                    </Tippy>
                </div>
                <div className="flex">
                    <AreaRadiusSelect />
                </div>
            </div>

        );
    }
}

export default NewAdLocationsContent;