import React from 'react';
import Modal from 'react-modal';
import FeatherIcon from 'feather-icons-react';

class ConfirmRemoveUser extends React.Component {

    render() {
        return (
            <Modal
                isOpen={this.props.isOpen}
                className={this.props.className}
                onRequestClose={this.props.modalClosed}
                shouldCloseOnOverlayClick={true}
            >
                <div className="popup-header">
                    <h2>Edit Name</h2>
                    <div className="close-popup" onClick={this.props.modalClosed}>
                        <FeatherIcon icon={"x"} />
                    </div>
                </div>
                <p>Are you sure you want to remove this user? All ads and agents assigned to this user will be reassigned to the owner.</p>
                <div className="popup-buttons">
                    <button className="close-popup btn-cancel" onClick={this.props.modalClosed}>Cancel</button>
                    <button className="close-popup btn-primary" onClick={this.props.modalClosed}>Confirm</button>
                </div>

            </Modal>
        );
    }
}

export default ConfirmRemoveUser;