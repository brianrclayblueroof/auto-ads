import React from 'react';
import Modal from 'react-modal';
import FeatherIcon from 'feather-icons-react';
import Tippy from '@tippy.js/react';

class AddUser extends React.Component {

    render() {
        return (
            <Modal
                isOpen={this.props.isOpen}
                className={this.props.className}
                onRequestClose={this.props.modalClosed}
                shouldCloseOnOverlayClick={true}
            >
                <div className="popup-header">
                    <h2>Add User</h2>
                    <div className="close-popup" onClick={this.props.modalClosed}>
                        <FeatherIcon icon={"x"} />
                    </div>
                </div>
                <div className="input-group">
                    <div className="input-wrapper">
                        <label>First Name</label>
                        <input placeholder="Enter First Name" type="text" />
                    </div>
                    <div className="input-wrapper">
                        <label>Last Name</label>
                        <input placeholder="Enter Last Name" type="text" />
                    </div>
                </div>
                <div className="input-group m-t-20">
                    <div className="input-wrapper">
                        <label>Email</label>
                        <input placeholder="Enter Email" type="email" />
                    </div>
                    <div className="input-wrapper">
                        <label>Phone</label>
                        <input placeholder="Enter Phone" type="tel" />
                    </div>
                </div>
                <label className="m-t-30">Permissions</label>
                <ul className="permissions-list m-t-10">
                    <li className="flex ai-center jc-between">
                    <div className="flex ai-center">
                        <label className="checkbox">
                        <input type="checkbox" />
                        <span className="checkmark" />
                        </label>
                        <span>Manage Ads</span>
                    </div>
                    <Tippy content="Allows this user to manage all settings related to an agents ads including creating, pausing, and deleting ads.">
                        <FeatherIcon icon="info" className="grey" width={20}/>
                    </Tippy>
                    </li>
                    <li className="flex ai-center jc-between">
                    <div className="flex ai-center">
                        <label className="checkbox">
                        <input type="checkbox" />
                        <span className="checkmark" />
                        </label>
                        <span>Manage Posts</span>
                    </div>
                    <Tippy content="Allows this user to manage all settings related to an agents posts including creating, pausing, and deleting posts.">
                        <FeatherIcon icon="info" className="grey" width={20}/>
                    </Tippy>
                    </li>
                    <li className="flex ai-center jc-between">
                    <div className="flex ai-center">
                        <label className="checkbox">
                        <input type="checkbox" />
                        <span className="checkmark" />
                        </label>
                        <span>Manage Templates</span>
                    </div>
                    <Tippy content="Allows this user to manage all settings related to an agents templates including creating and deleting templates.">
                        <FeatherIcon icon="info" className="grey" width={20}/>
                    </Tippy>
                    </li>
                    <li className="flex ai-center jc-between">
                    <div className="flex ai-center">
                        <label className="checkbox">
                        <input type="checkbox" />
                        <span className="checkmark" />
                        </label>
                        <span>Manage Auto Ads</span>
                    </div>
                    <Tippy content="Allows this user to manage all settings related to an agents auto ads including creating and deleting auto ads.">
                        <FeatherIcon icon="info" className="grey" width={20}/>
                    </Tippy>
                    </li>
                    <li className="flex ai-center jc-between">
                    <div className="flex ai-center">
                        <label className="checkbox">
                        <input type="checkbox" />
                        <span className="checkmark" />
                        </label>
                        <span>Manage Users</span>
                    </div>
                    <Tippy content="Allows this user to delete and reset the password of all other users with the exception of the owner.">
                        <FeatherIcon icon="info" className="grey" width={20}/>
                    </Tippy>
                    </li>
                </ul>



                <div className="popup-buttons">
                    <button className="close-popup btn-cancel" onClick={this.props.modalClosed}>Cancel</button>
                    <button className="close-popup btn-primary" onClick={this.props.modalClosed}>Confirm</button>
                </div>

            </Modal>
        );
    }
}

export default AddUser;