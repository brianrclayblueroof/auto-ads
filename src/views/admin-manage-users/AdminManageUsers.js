import React, { Component } from 'react';
import TopNav from '../shared/top-nav/TopNav';
import AdminSideNav from '../shared/AdminSideNav';
import UserPermissions from '../shared/user-permissions/UserPermissions';
import FeatherIcon from 'feather-icons-react';
import Tippy from '@tippy.js/react';
import ConfirmRemoveUser from './popups/ConfrimRemoveUser';
import AddUser from './popups/AddUser';
import Button from '../shared/Button';

class AdminManageUsers extends Component {
  constructor(props){
    super(props)
    this.state = {
      confirmRemoveUserOpen: false,
      addUserOpen: false
    }
    this.removeUserOpen = this.removeUserOpen.bind(this);
    this.addUserOpen = this.addUserOpen.bind(this);
    this.closeModal = this.closeModal.bind(this);
  }

  addUserOpen() {
    this.setState({addUserOpen: true});
    console.log("hello")
  }

  removeUserOpen() {
    this.setState({confirmRemoveUserOpen: true});
    console.log("hello")
  }

  closeModal() {
    this.setState({confirmRemoveUserOpen: false});
    this.setState({addUserOpen: false});
  }

  render() {
    return (
      <div>
        <TopNav />
        <AdminSideNav />
        <div className="page-content">
          <div className="panel flex jc-between ai-center tool-bar m-b-20">
            <div className="tool-bar-left flex ai-center flex1">
              <div className="input-wrapper m-r-10 w-30">
                <div className="input-btn search">
                  <input placeholder="Search" />
                  <div>
                    <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} viewbox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth={2} strokeLinecap="round" strokeLinejoin="round" className="feather feather-search"><circle cx={11} cy={11} r={8} /><line x1={21} y1={21} x2="16.65" y2="16.65" /></svg>
                  </div>
                </div>
                <div className="input-message">
                  <img alt="" src="/assets/icons/check-circle-green.svg" />
                  <span>This is a success message</span>
                </div>
              </div>
            </div>
            <div className="tool-bar-right">
              <Button 
                buttonText={"Add User"}
                className="btn-primary"
                onClick={this.addUserOpen}
              />
            </div>
          </div>
          {/* Empty State */}
          <div className="panel hide">
            <div className="callout primary">
              You haven’t added any campaigns yet. Login as an agent and create an ad.
            </div>
          </div>
          <div className="panel w-100">
            <table id="users-table">
              <thead>
                <tr>
                  <th className="users-table-name">
                    <span>Name</span>
                  </th>
                  <th className="users-table-username">
                    <span>Username</span>
                  </th>
                  <th className="users-table-email">
                    <span>Email</span>
                  </th>
                  <th className="users-table-phone">
                    <span>Phone</span>
                  </th>
                  <th className="users-table-permissions">
                    <span>Permissions</span>
                  </th>
                  <th className="users-table-actions" data-sortable="false" />
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td className="users-table-name">
                    <div className="flex ai-center">
                      <div className="flex ai-center">
                        <div className="table-avatar m-r-15" style ={ { backgroundImage: "url("+require('../../assets/images/placeholder/profile-img.png')+")" } }/>
                        <span>Jason Clay</span>
                      </div>
                    </div>
                  </td>
                  <td className="users-table-username">
                    <p>jason.c</p>
                  </td>
                  <td className="users-table-email">
                    <p>jason.c@gmail.com</p>
                  </td>
                  <td className="users-table-phone">
                    <p>919-710-2117</p>
                  </td>
                  <td className="users-table-permissions">
                    <UserPermissions 
                      triggerLabel="Ads, Posts, Auto Ads"
                    />
                  </td>
                  <td className="users-table-actions actions">
                    <div className="flex ai-center jc-end">
                      <p className="pointer blue m-r-15">Reset Password</p>
                      <FeatherIcon icon="trash-2" onClick={this.removeUserOpen}/>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td className="users-table-name">
                    <div className="flex ai-center">
                      <div className="flex ai-center">
                        <div className="table-avatar m-r-15" style ={ { backgroundImage: "url("+require('../../assets/images/placeholder/profile-img.png')+")" } } />
                        <span>Brian Clay (Owner)</span>
                      </div>
                    </div>
                  </td>
                  <td className="users-table-username">
                    <p>brian.c</p>
                  </td>
                  <td className="users-table-email">
                    <p>brian.c@gmail.com</p>
                  </td>
                  <td className="users-table-phone">
                    <p>919-710-2116</p>
                  </td>
                  <td className="users-table-permissions">
                    <UserPermissions 
                      triggerLabel="All"
                    />
                  </td>
                  <td className="users-table-actions actions">
                    <div className="flex ai-center jc-end">
                      <p className="pointer blue m-r-15">Reset Password</p>
                      <Tippy content="You can't remove owners">
                        <FeatherIcon icon="trash-2" className="inactive"/>
                      </Tippy>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td className="users-table-name">
                    <div className="flex ai-center">
                      <div className="flex ai-center">
                        <div className="table-avatar m-r-15" style ={ { backgroundImage: "url("+require('../../assets/images/placeholder/profile-img.png')+")" } } />
                        <span>David Clay</span>
                      </div>
                    </div>
                  </td>
                  <td className="users-table-username">
                    <p>david.c</p>
                  </td>
                  <td className="users-table-email">
                    <p>david.c@gmail.com</p>
                  </td>
                  <td className="users-table-phone">
                    <p>919-710-2118</p>
                  </td>
                  <td className="users-table-permissions">
                    <UserPermissions 
                      triggerLabel={"Posts, Ads"}
                    />
                  </td>
                  <td className="users-table-actions actions">
                    <div className="flex ai-center jc-end">
                      <p className="pointer blue m-r-15">Reset Password</p>
                      <FeatherIcon icon="trash-2" onClick={this.removeUserOpen}/>
                    </div>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
        <ConfirmRemoveUser 
          isOpen = {this.state.confirmRemoveUserOpen}
          modalClosed = {this.closeModal}
        />
        <AddUser 
          isOpen = {this.state.addUserOpen}
          modalClosed = {this.closeModal}
        />
      </div>
    );
  }
}

export default AdminManageUsers;