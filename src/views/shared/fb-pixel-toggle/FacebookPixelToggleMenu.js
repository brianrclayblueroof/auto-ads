import React, { Component } from 'react';

class FacebookPixelToggleMenu extends Component {

    render() {
        return (
            <ul className="dropdown-menu checkbox-menu">
                <li className="dropdown-item">
                    <label className="radio">
                        <div className="flex ai-center">
                            <div className="status running m-r-10" />
                            Enabled
                        </div>
                        <input type="radio" name="fb-pixel" defaultChecked="true" />
                        <span className="radio-indicator" />
                    </label>
                </li>
                <li className="dropdown-item">
                    <label className="radio">
                        <div className="flex ai-center">
                            <div className="status error m-r-10" />
                            Disabled
                        </div>
                        <input type="radio" name="fb-pixel" />
                        <span className="radio-indicator" />
                    </label>
                </li>
            </ul>
        );
    }
}

export default FacebookPixelToggleMenu;