import React, { Component, Fragment } from 'react';
import MatchFieldsIcon from './MatchFieldsIcon';
import MatchFieldLabel from './MatchFieldLabel';

class AdContentText extends Component {
    render() {
        return (
            <Fragment>
                <MatchFieldLabel label="Text"/>
                <textarea placeholder="Enter text that clearly tells people about what you're promoting"></textarea>
            </Fragment>
        );
    }
}

export default AdContentText;