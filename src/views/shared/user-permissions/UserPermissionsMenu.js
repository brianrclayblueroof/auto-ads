import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import FeatherIcon from 'feather-icons-react';
    
class UserPermissionsMenu extends Component {
    constructor(props) {
        super(props)
        this.state = {
            adStatusEnabled: true
        }
    }

    toggleAdminUser(e) {
        console.log('hi')
    }

    render() {
        return (
            <ul className="dropdown-menu checkbox-menu">
                <li className={"dropdown-item"}>
                    <label className="checkbox">
                        <input type="checkbox" defaultChecked={true} />
                        <span className="checkmark" />
                    </label>
                    <span>Manage Ads</span>
                </li>
                <li className={"dropdown-item"}>
                    <label className="checkbox">
                        <input type="checkbox" defaultChecked={true} />
                        <span className="checkmark" />
                    </label>
                    <span>Manage Posts</span>
                </li>
                <li className={"dropdown-item"}>
                    <label className="checkbox">
                        <input type="checkbox" defaultChecked={true} />
                        <span className="checkmark" />
                    </label>
                    <span>Manage Templates</span>
                </li>
                <li className={"dropdown-item"}>
                    <label className="checkbox">
                        <input type="checkbox" defaultChecked={true} />
                        <span className="checkmark" />
                    </label>
                    <span>Manage Auto Ads</span>
                </li>
                <li className={"dropdown-item"}>
                    <label className="checkbox">
                        <input type="checkbox" defaultChecked={true} />
                        <span className="checkmark" />
                    </label>
                    <span>Manage Users</span>
                </li>
            </ul>
        );
    }
}

export default UserPermissionsMenu;