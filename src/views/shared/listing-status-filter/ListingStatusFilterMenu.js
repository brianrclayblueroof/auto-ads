import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import FeatherIcon from 'feather-icons-react';

const listingStatuses = [
    {
        status: <span className="status-label">All</span>
    },
    {
        status: <span className="status-label">New Listings</span>
    },
    {
        status: <span className="status-label">Active</span>
    },
    {
        status: <span className="status-label">Active with Contingency</span>
    },
    {
        status: <span className="status-label">Expired</span>
    },
    {
        status: <span className="status-label">Pending</span>
    },
    {
        status: <span className="status-label">Sold</span>
    }
]
    
class ListingStatusFilterMenu extends Component {
    constructor(props) {
        super(props)
        this.state = {
            adStatusEnabled: true
        }
    }

    toggleAdminUser(e) {
        console.log('hi')
    }

    render() {
        return (
            <ul className="dropdown-menu checkbox-menu">
                {listingStatuses.map(listingStatus => (
                    <li key={{listingStatus}} className="dropdown-item checkbox-item" onClick={()=>{this.toggleAdminUser()}}>
                        <label className="checkbox">
                            <input type="checkbox" defaultChecked={this.state.adStatusEnabled} />
                            <span className="checkmark" />
                        </label>
                        {listingStatus.status}
                    </li>
                ))}
            </ul>
        );
    }
}

export default ListingStatusFilterMenu;