import React from 'react';
const listingdata = {
    listings: [
        {
          mlsID: "1234122",
          streetAddress: "123 S State St.",
          city: "Orem",
          state: 'UT',
          zip: '84005',
          price: '$800,000',
          bedCount: '3',
          bathCount: '2',
          status: 'New',
          primaryImg: null,
          adStatus: 'running',
          selected: false
        },
        {
          mlsID: "1234122",
          streetAddress: "123 S State St.",
          city: "Provo",
          state: 'UT',
          zip: '84005',
          price: '$700,000',
          bedCount: '3',
          bathCount: '2',
          status: 'Under Contract',
          primaryImg: 'https://www.wsbradio.com/rw/Wires/w2/Local_Wsbtv/2018/06/16/Images/770984640_house%25202_1529153807906.jpg_12009876_ver1.0_640_360.jpg',
          adStatus: 'paused',
          selected: false
        },
        {
          mlsID: "1234122",
          streetAddress: "123 S State St.",
          city: "Orem",
          state: 'UT',
          zip: '84005',
          price: '$600,000',
          bedCount: '4',
          bathCount: '2',
          status: 'Active',
          primaryImg: null,
          adStatus: 'error',
          selected: false
        },
        {
          mlsID: "1234122",
          streetAddress: "123 S State St.",
          city: "Orem",
          state: 'UT',
          zip: '84005',
          price: '$600,000',
          bedCount: '4',
          bathCount: '2',
          status: 'Active',
          primaryImg: null,
          adStatus: 'ended',
          selected: false
        },
        {
          mlsID: "1234122",
          streetAddress: "123 S State St.",
          city: "Orem",
          state: 'UT',
          zip: '84005',
          price: '$600,000',
          bedCount: '4',
          bathCount: '2',
          status: 'Active',
          primaryImg: null,
          adStatus: 'pending',
          selected: false
        },
      ]

}

export default listingdata