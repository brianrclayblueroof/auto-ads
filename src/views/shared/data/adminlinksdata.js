import React from 'react';
const linksData = {
    AdminSideNavLinks: [
        {
          name: "Agents",
          link: '/',
          isInactive: false,
          isActive: true,
          img: <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} viewbox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth={2} strokeLinecap="round" strokeLinejoin="round" className="feather feather-users"><path d="M17 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2" /><circle cx={9} cy={7} r={4} /><path d="M23 21v-2a4 4 0 0 0-3-3.87" /><path d="M16 3.13a4 4 0 0 1 0 7.75" /></svg>
        },
        {
          name: "Campaigns",
          link: '/admin-campaigns',
          isInactive: false,
          isActive:false,
          img:  <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} viewbox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth={2} strokeLinecap="round" strokeLinejoin="round" className="feather feather-users"><path d="M17 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2" /><circle cx={9} cy={7} r={4} /><path d="M23 21v-2a4 4 0 0 0-3-3.87" /><path d="M16 3.13a4 4 0 0 1 0 7.75" /></svg>
        },
        {
          name: "Posts",
          link: '/admin-posts',
          isInactive: true,
          isActive:false,
          img: <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} viewbox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth={2} strokeLinecap="round" strokeLinejoin="round" className="feather feather-users"><path d="M17 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2" /><circle cx={9} cy={7} r={4} /><path d="M23 21v-2a4 4 0 0 0-3-3.87" /><path d="M16 3.13a4 4 0 0 1 0 7.75" /></svg>
        },
        {
          name: "Templates",
          isInactive: false,
          isActive: false,
          link: '/admin-templates',
          img: <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} viewbox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth={2} strokeLinecap="round" strokeLinejoin="round" className="feather feather-layers"><polygon points="12 2 2 7 12 12 22 7 12 2" /><polyline points="2 17 12 22 22 17" /><polyline points="2 12 12 17 22 12" /></svg>
        },
        {
          name: "Auto Ads",
          isInactive: true,
          isActive: false,
          link: '/admin-auto-ads',
          img: <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} viewbox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth={2} strokeLinecap="round" strokeLinejoin="round" className="feather feather-zap"><polygon points="13 2 3 14 12 14 11 22 21 10 12 10 13 2" /></svg>
        },
      ]

}

export default linksData