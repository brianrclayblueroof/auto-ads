import React, { Component } from 'react';
import Tippy from '@tippy.js/react';
import FeatherIcon from 'feather-icons-react';

class FacebookAdPreview extends Component {
    render() {
        return (
            <div className="panel fb-ad-preview-wrapper">
                <label>Ad Preview</label>
                <div className="fb-ad-preview single-img">
                    <div className="preview-top flex ai-start">
                        <div className="preview-avatar" style ={ { backgroundImage: "url("+require('../../../assets/images/placeholder/profile-img.png')+")" } } />
                        <div className="profile-info">
                            <p className="profile-name facebook">Primary FB Profile Name</p>
                            <div className="sponsored flex ai-center">
                                <p className="grey m-r-10">Sponsored</p>
                                <div className="sponsored-dot" />
                                <img className="public-icon" src={require("../../../assets/icons/fb-public.png")} />
                            </div>
                        </div>
                    </div>
                    <p className="preview-text">
                        Beautiful new listing at {'{'}{'{'}LISTING ADDRESS{'}'}{'}'}. You do not want to miss out on this one! Contact me today for more info! {'{'}{'{'}AGENT PHONE{'}'}{'}'}
                    </p>
                    <div className="preview-img">
                        <img src="/assets/icons/image-grey.svg" />
                        <p className="preview-img-text grey">First image of first listing</p>
                    </div>
                    <div className="preview-bottom">
                        <div className="preview-cta-info">
                            <p className="preview-URL">{'{'}{'{'}Agents Homepage URL{'}'}{'}'}</p>
                            <p className="preview-headline">{'{'}{'{'}LISTING ADDRESS{'}'}{'}'}</p>
                            <p className="preview-description">{'{'}{'{'}LISTING PRICE{'}'}{'}'} | {'{'}{'{'}LISTING BEDS{'}'}{'}'} beds | {'{'}{'{'}LISTING BATHS{'}'}{'}'} baths</p>
                        </div>
                        <button className="preview-cta learn-more">Learn More</button>
                    </div>
                    <div className="share-btns flex ai-center jc-between p-10 m-t-10">
                        <img src="/assets/icons/fb-like.png" />
                        <img src="/assets/icons/fb-comment.png" />
                        <img src="/assets/icons/fb-share.png" />
                    </div>
                </div>
                <div className="preview-ations flex ai-center">
                    <div className="edit-content inactive pointer flex ai-center w-100 jc-center">
                        <svg xmlns="http://www.w3.org/2000/svg" width={18} height={18} viewBox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth={2} strokeLinecap="round" strokeLinejoin="round" className="feather feather-edit-3">
                            <polygon points="14 2 18 6 7 17 3 17 3 13 14 2" />
                            <line x1={3} y1={22} x2={21} y2={22} />
                        </svg>
                        <span className="m-l-10">Edit Content</span>
                    </div>
                    <div className="refresh-preview inactive pointer flex ai-center w-100 jc-center">
                        <svg xmlns="http://www.w3.org/2000/svg" width={18} height={18} viewBox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth={2} strokeLinecap="round" strokeLinejoin="round" className="feather feather-rotate-ccw">
                            <polyline points="1 4 1 10 7 10" />
                            <path d="M3.51 15a9 9 0 1 0 2.13-9.36L1 10" />
                        </svg>
                        <span className="m-l-10">Refresh Preview</span>
                    </div>
                </div>
            </div>

        );
    }
}

export default FacebookAdPreview;