import React, { Component, Fragment } from 'react';
import MatchFieldsIcon from './MatchFieldsIcon';
import Tippy from '@tippy.js/react';
import FeatherIcon from 'feather-icons-react';

class MatchFieldLabel extends Component {
    render() {
        return (
            <div className="match-field-label">
                <div className="help-label">
                    <label>{this.props.label}</label>
                    {this.props.helpText ?
                        <Tippy content={this.props.helpText}>
                            <FeatherIcon icon="info" className="blue m-l-5" width={16} />
                        </Tippy>
                        : 
                        ""
                    }
                </div>
                <MatchFieldsIcon />
            </div>
        );
    }
}

export default MatchFieldLabel;