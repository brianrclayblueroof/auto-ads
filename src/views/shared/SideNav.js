import React, { Component } from 'react';
import { Link } from 'react-router-dom'

class SideNav extends Component {
    render() {
        return (
            <div className="side-nav admin">
                <div className="links">
                    <Link to='/'>
                        <div className="link inactive">
                            <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} viewbox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth={2} strokeLinecap="round" strokeLinejoin="round" className="feather feather-users"><path d="M17 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2" /><circle cx={9} cy={7} r={4} /><path d="M23 21v-2a4 4 0 0 0-3-3.87" /><path d="M16 3.13a4 4 0 0 1 0 7.75" /></svg>
                            <span>Dashboard</span>
                        </div>
                    </Link>
                    <Link to='/listings'>
                        <div className="link active">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24" fill="none"
                                stroke="currentColor" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" className="feather feather-home">
                                <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                                <polyline points="9 22 9 12 15 12 15 22"></polyline>
                            </svg>
                            <span>Listings</span>
                        </div>
                    </Link>
                    <Link to='/my-ads'>
                        <div className="link">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24" fill="none"
                                stroke="currentColor" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" className="feather feather-flag">
                                <path d="M4 15s1-1 4-1 5 2 8 2 4-1 4-1V3s-1 1-4 1-5-2-8-2-4 1-4 1z"></path>
                                <line x1="4" y1="22" x2="4" y2="15"></line>
                            </svg>
                            <span>My Ads</span>
                        </div>
                    </Link>
                    <Link to='/admin-templates'>
                        <div className="link inactive">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24" fill="none"
                                stroke="currentColor" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" className="feather feather-file">
                                <path d="M13 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V9z"></path>
                                <polyline points="13 2 13 9 20 9"></polyline>
                            </svg>
                            <span>Posts</span>
                        </div>
                    </Link>
                    <Link to='/templates'>
                        <div className="link inactive">
                            <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} viewbox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth={2} strokeLinecap="round" strokeLinejoin="round" className="feather feather-layers"><polygon points="12 2 2 7 12 12 22 7 12 2" /><polyline points="2 17 12 22 22 17" /><polyline points="2 12 12 17 22 12" /></svg>
                            <span>Templates</span>
                        </div>
                    </Link>
                    <Link to='/auto-ads'>
                        <div className="link inactive">
                            <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} viewbox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth={2} strokeLinecap="round" strokeLinejoin="round" className="feather feather-zap"><polygon points="13 2 3 14 12 14 11 22 21 10 12 10 13 2" /></svg>
                            <span>Auto Ads</span>
                        </div>
                    </Link>
                </div>
            </div>
        );
    }
}

export default SideNav;