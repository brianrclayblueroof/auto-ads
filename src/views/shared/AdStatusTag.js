import React, { Component } from 'react';

class AdStatusTag extends Component {
    render() {
        if(this.props.tagType == "running"){
            return (
                <div className="ad-status-tag running">{this.props.tagType}</div>
            );
        }
        if(this.props.tagType == "paused"){
            return (
                <div className="ad-status-tag paused">{this.props.tagType}</div>
            );
        }
        if(this.props.tagType == "error"){
            return (
                <div className="ad-status-tag error">{this.props.tagType}</div>
            );
        }
        if(this.props.tagType == "pending"){
            return (
                <div className="ad-status-tag pending">{this.props.tagType}</div>
            );
        }
        if(this.props.tagType == "ended"){
            return (
                <div className="ad-status-tag ended">{this.props.tagType}</div>
            );
        }
        return null
    }
}

export default AdStatusTag;