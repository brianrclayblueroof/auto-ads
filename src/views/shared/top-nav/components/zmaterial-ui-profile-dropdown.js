import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import MenuItem from '@material-ui/core/MenuItem';
import FeatherIcon from 'feather-icons-react';
import MenuList from '@material-ui/core/MenuList';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import Grow from '@material-ui/core/Grow';
import Paper from '@material-ui/core/Paper';
import Popper from '@material-ui/core/Popper';
import ProfileAvatar from './ProfileAvatar';
import { Link } from 'react-router-dom';

class ProfileDropdown extends Component {
    state = {
        open: false,
    };

    handleToggle = () => {
        this.setState(state => ({ open: !state.open }));
    };

    handleClose = event => {
        if (this.anchorEl.contains(event.target)) {
            return;
        }

        this.setState({ open: false });
    };

    render() {
        const { open } = this.state;

        return (
            <div className="profile-dropdown dropdown">
                <Button
                    buttonRef={node => {
                        this.anchorEl = node;
                    }}
                    aria-owns={open ? 'menu-list-grow' : undefined}
                    aria-haspopup="true"
                    onClick={this.handleToggle}
                    className="text-btn btn"
                    disableFocusRipple
                    disableTouchRipple
                >
                    <ProfileAvatar />
                    <span className="profile-dropdown-label">Hi Brian</span>
                    <svg className="carret" width="10" height="6" viewbox="0 0 10 6" fill="none" xmlns="http://www.w3.org/2000/svg"> <path d="M5 6L0.669873 -8.15666e-07L9.33013 -5.85621e-08L5 6Z" fill="currentColor" /> </svg>
                </Button>
                <Popper open={open} anchorEl={this.anchorEl} transition disablePortal>
                    {({ TransitionProps, placement }) => (
                        <Grow
                            {...TransitionProps}
                            id="menu-list-grow"
                            style={{ transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom' }}
                        >
                            <Paper>
                                <ClickAwayListener onClickAway={this.handleClose}>
                                    <MenuList>
                                        <Link to="/my-profile">
                                            <MenuItem 
                                                disableFocusRipple
                                                disableTouchRipple
                                            >
                                                <ListItemIcon>
                                                    <FeatherIcon icon={'user'} />
                                                </ListItemIcon>
                                                <ListItemText inset primary="My Profile" />
                                            </MenuItem>
                                        </Link>
                                        <Link to="/manage-users">
                                            <MenuItem 
                                                disableFocusRipple
                                                disableTouchRipple
                                            >
                                                <ListItemIcon>
                                                    <FeatherIcon icon={'users'} />
                                                </ListItemIcon>
                                                <ListItemText inset primary="Manage Users" />
                                            </MenuItem>
                                        </Link>
                                        <Link to="/">
                                            <MenuItem 
                                                disableFocusRipple
                                                disableTouchRipple
                                            >
                                                <ListItemIcon>
                                                    <FeatherIcon icon={'log-out'} />
                                                </ListItemIcon>
                                                <ListItemText inset primary="Logout" />
                                            </MenuItem>
                                        </Link>
                                    </MenuList>
                                </ClickAwayListener>
                            </Paper>
                        </Grow>
                    )}
                </Popper>
            </div>
        );
    }
}

export default ProfileDropdown;