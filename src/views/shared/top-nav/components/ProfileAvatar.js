import React from 'react'

const ProfileAvatar = () => {
    return(
        <div className="top-avatar" style={{ backgroundImage: "url(" + require('../../../../assets/images/placeholder/profile-img.png') + ")" }} />
    )
}

export default ProfileAvatar