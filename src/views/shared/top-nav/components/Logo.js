import React from 'react';

const Logo = () => {
    return (
        <div className="logo">
            <img alt="" src={require('../../../../assets/images/brlogo.svg')} />
        </div>
    );
}

export default Logo;