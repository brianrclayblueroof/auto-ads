import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import FeatherIcon from 'feather-icons-react';

class ProfileDropdownMenu extends Component {
    constructor(props) {
        super(props)
        this.state = {
            dropdownOpen: false
        }
    }

    render() {
        return (
            <div className="dropdown-menu">
                <Link to="/my-profile" className="dropdown-item">
                    <FeatherIcon icon={'user'} />
                    <span className="item-label">My Profile</span>
                </Link>
                <Link to="/manage-users" className="dropdown-item">
                    <FeatherIcon icon={'users'} />
                    <span className="item-label">Manage Users</span>
                </Link>
                <Link to="/my-profile" className="dropdown-item">
                    <FeatherIcon icon="log-out" />
                    <span className="item-label">Logout</span>
                </Link>
            </div>
        );
    }
}

export default ProfileDropdownMenu;