import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Tippy from '@tippy.js/react';
import 'tippy.js/dist/tippy.css';
import userprofiledata from '../../data/userprofiledata';

class LogoutAsAgent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isAdmin: userprofiledata.isAdmin
        }
    }

    render() {
        return (
            <Link to="/">
                <Tippy content={<span>Click to logout</span>}>
                    <span className={this.state.isAdmin ? `m-l-30 blue` : `hide`}>Logged in as Agent</span>
                </Tippy>
            </Link>
        );
    }
}

export default LogoutAsAgent;