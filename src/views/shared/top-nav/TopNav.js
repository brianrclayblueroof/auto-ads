import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Tippy from '@tippy.js/react';
import 'tippy.js/dist/tippy.css';
import userprofiledata from '../data/userprofiledata';
import Logo from './components/Logo';
import LogoutAsAgent from './components/LogoutAsAgent';
import ProfileDropdown from './components/ProfileDropdown';

class TopNav extends Component {
  constructor(props){
    super(props)
    this.state = {
      isAdmin: userprofiledata.isAdmin
    }
  }

  render() {
    return (
      <div className="top-nav">
        <div className="top-nav-left flex ai-center">
          <Logo />
          <LogoutAsAgent />
        </div>
        <div className="top-nav-right">
          <ProfileDropdown />
        </div>
      </div>

    );
  }
}

export default TopNav;