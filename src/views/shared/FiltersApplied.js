import React, { Component } from 'react';
import Tippy from '@tippy.js/react';
import FeatherIcon from 'feather-icons-react';

class FiltersApplied extends Component {
    render() {
        return (
            <div className="m-l-20 blue flex ai-center">
                <span className="m-r-10">1 Filter Applied</span>
                <Tippy content={<span>Remove Filters</span>}>
                    <FeatherIcon icon={'x'} width={22} className="pointer"/>
                </Tippy>
            </div>

        );
    }
}

export default FiltersApplied;