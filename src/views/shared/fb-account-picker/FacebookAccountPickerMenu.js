import React, { Component } from 'react';

class FacebookAccountPickerMenu extends Component {

    render() {
        return (
            <ul className="dropdown-menu checkbox-menu">
                <li className="dropdown-item">
                    <label className="radio">
                        <div className="flex ai-center">
                            <div className="table-avatar m-r-10"
                                style={{ backgroundImage: "url(" + require('../../../assets/images/placeholder/profile-img.png') + ")" }} />
                            Brian Clay Realty
                        </div>
                        <input type="radio" name="fb-page" defaultChecked="true" />
                        <span className="radio-indicator" />
                    </label>
                </li>
                <li className="dropdown-item">
                    <label className="radio">
                        <div className="flex ai-center">
                            <div className="table-avatar m-r-10"
                                style={{ backgroundImage: "url(" + require('../../../assets/images/placeholder/profile-img.png') + ")" }} />
                            Brian Smith Realty
                        </div>
                        <input type="radio" name="fb-page" />
                        <span className="radio-indicator" />
                    </label>
                </li>
            </ul>
        );
    }
}

export default FacebookAccountPickerMenu;