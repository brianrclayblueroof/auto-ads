import React, { Component } from 'react';
import Tippy from '@tippy.js/react';
import 'tippy.js/dist/tippy.css';
import FacebookAccountPickerMenu from './FacebookAccountPickerMenu';


class FacebookAccountPicker extends Component {
    render() {

        return (
            <div className="profile-dropdown dropdown">
                <Tippy content={<FacebookAccountPickerMenu />} trigger="click" interactive placement={"bottom-start"}>
                    <div class="dropdown-trigger btn jc-between">
                        <div className="flex ai-center">
                            <div className="table-avatar m-r-10"
                                style={{ backgroundImage: "url(" + require('../../../assets/images/placeholder/profile-img.png') + ")" }} />
                            <span>Brian Clay Realty</span>
                        </div>
                        <svg className="carret" width="10" height="6" viewbox="0 0 10 6" fill="none" xmlns="http://www.w3.org/2000/svg"> <path d="M5 6L0.669873 -8.15666e-07L9.33013 -5.85621e-08L5 6Z" fill="currentColor" /> </svg>
                    </div>
                </Tippy>
            </div>
        );
    }
}

export default FacebookAccountPicker;