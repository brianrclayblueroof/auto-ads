import React, { Component } from 'react';
import FeatherIcon from 'feather-icons-react';
import listingdata from '../data/listingdata';
import { Link } from 'react-router-dom'
import Tippy from '@tippy.js/react';


class ListingPreivew extends Component {
    constructor(props) {
        super(props)
        this.state = {
            listings: listingdata.listings
        }
    }
    render() {
        const listing = this.state.listings[0];
        return (
            <div className="hide">
                <div className="side-preview">
                    <div className="listing-preview-img" style={{ backgroundImage: "url(" + require('../../../assets/images/placeholder/listing-thumbnail-large.png') + ")" }}>
                        <div className="close-preview">
                            <FeatherIcon icon={"x"} />
                        </div>
                    </div>
                    <div className="preview-info">
                        <div className="listing-info flex ai-start jc-between m-t-20">
                            <div className="listing-address">
                                <h2>{listing.streetAddress}</h2>
                                <p>{listing.city}, {listing.state} {listing.zip}</p>
                            </div>
                            <p class="preview-mls">MLS#: 1234121</p>
                        </div>
                        <div className="listing-details flex ai-center m-t-15 m-b-40">
                            <span className="listing-price">{listing.price}</span>
                            <div className="dot" />
                            <span className="listing-beds">{listing.bedCount} bd</span>
                            <div className="dot" />
                            <span className="listing-baths">{listing.bathCount} ba</span>
                            <div className="dot" />
                            <span className="listing-status">{listing.status}</span>
                        </div>
                    </div>
                    <div className="active-auto-ads p-l-20 p-r-20">
                        <div className="preview-header">
                            <h3>Active Auto Ads</h3>
                        </div>
                        <ul className="active-auto-ads-list">
                            <li className="flex ai-start jc-between">
                                <div>
                                    <p>New Listing</p>
                                    <p className="next-step grey">Next Step: Change Listing Details</p>
                                </div>
                                <div class="preview-actions flex ai-center">
                                    <Link to="/edit-ad" className="flex">
                                        <FeatherIcon icon="edit-3" width={20} className="m-r-10" />
                                    </Link>
                                    <FeatherIcon icon="trash-2" width={20} />
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div className="active-ads p-l-20 p-r-20 m-t-60">
                        <div className="preview-header flex ai-center jc-between">
                            <h3>Active Ads</h3>
                            <Link to="/new-ad" className="blue">
                                New Ad
                            </Link>
                        </div>
                        <ul className="active-ads-list">
                            <li className="flex ai-start jc-between">
                                <div>
                                    <div className="flex ai-center">
                                        <Tippy content="Running">
                                            <div class="status running m-r-10" />
                                        </Tippy>
                                        <span>Boost Mangum's Home</span>
                                    </div>
                                    <div className="ad-details m-t-5 flex ai-center">
                                        <div className="preview-platforms flex ai-center">
                                            <Tippy content="Facebook">
                                                <FeatherIcon icon="facebook" width={16} />
                                            </Tippy>
                                            <Tippy content="Instagram">
                                                <FeatherIcon icon="instagram" width={16} />
                                            </Tippy>
                                        </div>
                                        <div className="dot" />
                                        <Tippy content="Duration">
                                            <div className="duration flex ai-center">
                                                <FeatherIcon icon="clock" width={16} />
                                                <span className="m-l-5">3 Days</span>
                                            </div>
                                        </Tippy>
                                        <div className="dot" />
                                        <Tippy content="Clicks">
                                            <div className="clicks flex ai-center">
                                                <svg width="14" height="14" viewbox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M13.8617 11.8411L11.0756 9.05495L13.0633 7.90681C13.2186 7.81706 13.309 7.64656 13.296 7.46767C13.2832 7.28873 13.1692 7.13294 13.0026 7.06652L4.13908 3.53092C3.96529 3.46153 3.76693 3.5024 3.63461 3.63467C3.50229 3.76704 3.46143 3.96539 3.53077 4.13915L7.06585 13.0036C7.13232 13.1702 7.28811 13.2842 7.46705 13.2971C7.64598 13.3101 7.81648 13.2197 7.90619 13.0643L9.05415 11.0767L11.8401 13.8629C11.9279 13.9507 12.047 14 12.1712 14C12.2953 14 12.4144 13.9507 12.5022 13.8629L13.8617 12.5032C14.0445 12.3204 14.0445 12.024 13.8617 11.8411ZM12.1712 12.8698L9.28493 9.98329C9.1965 9.89486 9.0772 9.84612 8.95388 9.84612C8.93361 9.84612 8.91316 9.84744 8.89284 9.85013C8.74845 9.86908 8.62118 9.95411 8.54845 10.0802L7.57815 11.7603L4.80411 4.80441L11.7593 7.57882L10.0791 8.5493C9.95303 8.62203 9.86799 8.74931 9.84904 8.89369C9.83004 9.03793 9.87921 9.18284 9.98216 9.28579L12.8685 12.1723L12.1712 12.8698Z" fill="black" />
                                                    <path d="M2.05507 1.39313C1.87218 1.21043 1.57586 1.21043 1.39287 1.39313C1.21003 1.57603 1.21003 1.87244 1.39287 2.05533L2.43317 3.09563C2.52457 3.18698 2.64444 3.23276 2.76427 3.23276C2.88404 3.23276 3.00382 3.18698 3.09532 3.09563C3.27812 2.91278 3.27812 2.61633 3.09532 2.43348L2.05507 1.39313Z" fill="black" />
                                                    <path d="M2.40763 4.62975C2.40763 4.3712 2.19796 4.16162 1.93941 4.16162H0.468224C0.209718 4.16162 0 4.37115 0 4.62975C0 4.88826 0.209671 5.09793 0.468224 5.09793H1.93941C2.19796 5.09788 2.40763 4.88826 2.40763 4.62975Z" fill="black" />
                                                    <path d="M2.21797 6.0746L1.17758 7.11499C0.994681 7.29775 0.994681 7.5942 1.17758 7.77705C1.26898 7.86845 1.38871 7.91417 1.50863 7.91417C1.62841 7.91417 1.74819 7.86845 1.83959 7.77705L2.88002 6.73671C3.06287 6.5539 3.06287 6.2574 2.88002 6.0746C2.69727 5.8918 2.40086 5.8918 2.21797 6.0746Z" fill="black" />
                                                    <path d="M4.62951 2.40773C4.88801 2.40773 5.09768 2.19806 5.09768 1.9395V0.468177C5.09768 0.209671 4.88806 0 4.62951 0C4.37095 0 4.16138 0.209624 4.16138 0.468177V1.93955C4.16138 2.19806 4.37091 2.40773 4.62951 2.40773Z" fill="black" />
                                                    <path d="M6.40479 3.01714C6.52471 3.01714 6.64449 2.97151 6.73589 2.88007L7.77614 1.83977C7.95903 1.65692 7.95903 1.36047 7.77614 1.17766C7.59333 0.994815 7.29679 0.994815 7.11408 1.17766L6.07378 2.21796C5.89093 2.40076 5.89093 2.69726 6.07378 2.88007C6.16518 2.97151 6.28501 3.01714 6.40479 3.01714Z" fill="black" />
                                                </svg>
                                                <span className="m-l-5">142</span>
                                            </div>
                                        </Tippy>
                                        <div className="dot" />
                                        <Tippy content="Total Budget">
                                            <div className="budget flex ai-center">
                                                <svg width="16" height="16" viewbox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M8 14.5C11.866 14.5 14.5 11.866 14.5 8C14.5 4.13401 11.866 1.5 8 1.5C4.13401 1.5 1.5 4.13401 1.5 8C1.5 11.866 4.13401 14.5 8 14.5ZM8 16C12.4183 16 16 12.4183 16 8C16 3.58172 12.4183 0 8 0C3.58172 0 0 3.58172 0 8C0 12.4183 3.58172 16 8 16Z" fill="black" />
                                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M8.33329 4.33333C8.33329 4.14924 8.18405 4 7.99996 4C7.81586 4 7.66663 4.14924 7.66663 4.33333V5.33333H7.16663C6.7688 5.33333 6.38727 5.49136 6.10597 5.77267C5.82466 6.05397 5.66663 6.4355 5.66663 6.83333C5.66663 7.23115 5.82466 7.61268 6.10597 7.89399C6.38727 8.17529 6.7688 8.33333 7.16663 8.33333H7.66663V9.99999H5.99996C5.81586 9.99999 5.66663 10.1492 5.66663 10.3333C5.66663 10.5174 5.81586 10.6667 5.99996 10.6667H7.66663V11.6667C7.66663 11.8508 7.81586 12 7.99996 12C8.18405 12 8.33329 11.8508 8.33329 11.6667V10.6667H8.83329C9.23112 10.6667 9.61265 10.5086 9.89395 10.2273C10.1753 9.94602 10.3333 9.56449 10.3333 9.16666C10.3333 8.76884 10.1753 8.38731 9.89395 8.106C9.61265 7.8247 9.23112 7.66666 8.83329 7.66666H8.33329V5.99999H9.66663C9.85072 5.99999 9.99996 5.85076 9.99996 5.66666C9.99996 5.48257 9.85072 5.33333 9.66663 5.33333H8.33329V4.33333ZM7.66663 5.99999H7.16663C6.94561 5.99999 6.73365 6.08779 6.57737 6.24407C6.42109 6.40035 6.33329 6.61231 6.33329 6.83333C6.33329 7.05434 6.42109 7.2663 6.57737 7.42258C6.73365 7.57886 6.94561 7.66666 7.16663 7.66666H7.66663V5.99999ZM8.33329 8.33333V9.99999H8.83329C9.05431 9.99999 9.26627 9.9122 9.42255 9.75592C9.57883 9.59964 9.66663 9.38768 9.66663 9.16666C9.66663 8.94565 9.57883 8.73369 9.42255 8.57741C9.26627 8.42113 9.05431 8.33333 8.83329 8.33333H8.33329Z" fill="black" />
                                                </svg>
                                                <span className="m-l-5">$1500</span>
                                            </div>
                                        </Tippy>
                                    </div>
                                </div>
                                <div class="preview-actions flex ai-center">
                                    <Link to="/edit-ad" className="flex">
                                        <FeatherIcon icon="edit-3" width={20} className="m-r-10" />
                                    </Link>
                                    <FeatherIcon icon="eye" width={20} />
                                </div>
                            </li>
                            <li className="flex ai-start jc-between">
                                <div>
                                    <div className="flex ai-center">
                                        <Tippy content="Paused">
                                            <div class="status paused m-r-10" />
                                        </Tippy>
                                        <span>Open House</span>
                                    </div>
                                    <div className="ad-details m-t-5 flex ai-center">
                                        <div className="preview-platforms flex ai-center">
                                            <Tippy content="Facebook">
                                                <FeatherIcon icon="facebook" width={16} />
                                            </Tippy>
                                            <Tippy content="Instagram">
                                                <FeatherIcon icon="instagram" width={16} />
                                            </Tippy>
                                        </div>
                                        <div className="dot" />
                                        <Tippy content="Duration">
                                            <div className="duration flex ai-center">
                                                <FeatherIcon icon="clock" width={16} />
                                                <span className="m-l-5">10 Days</span>
                                            </div>
                                        </Tippy>
                                        <div className="dot" />
                                        <Tippy content="Clicks">
                                            <div className="clicks flex ai-center">
                                                <svg width="14" height="14" viewbox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M13.8617 11.8411L11.0756 9.05495L13.0633 7.90681C13.2186 7.81706 13.309 7.64656 13.296 7.46767C13.2832 7.28873 13.1692 7.13294 13.0026 7.06652L4.13908 3.53092C3.96529 3.46153 3.76693 3.5024 3.63461 3.63467C3.50229 3.76704 3.46143 3.96539 3.53077 4.13915L7.06585 13.0036C7.13232 13.1702 7.28811 13.2842 7.46705 13.2971C7.64598 13.3101 7.81648 13.2197 7.90619 13.0643L9.05415 11.0767L11.8401 13.8629C11.9279 13.9507 12.047 14 12.1712 14C12.2953 14 12.4144 13.9507 12.5022 13.8629L13.8617 12.5032C14.0445 12.3204 14.0445 12.024 13.8617 11.8411ZM12.1712 12.8698L9.28493 9.98329C9.1965 9.89486 9.0772 9.84612 8.95388 9.84612C8.93361 9.84612 8.91316 9.84744 8.89284 9.85013C8.74845 9.86908 8.62118 9.95411 8.54845 10.0802L7.57815 11.7603L4.80411 4.80441L11.7593 7.57882L10.0791 8.5493C9.95303 8.62203 9.86799 8.74931 9.84904 8.89369C9.83004 9.03793 9.87921 9.18284 9.98216 9.28579L12.8685 12.1723L12.1712 12.8698Z" fill="black" />
                                                    <path d="M2.05507 1.39313C1.87218 1.21043 1.57586 1.21043 1.39287 1.39313C1.21003 1.57603 1.21003 1.87244 1.39287 2.05533L2.43317 3.09563C2.52457 3.18698 2.64444 3.23276 2.76427 3.23276C2.88404 3.23276 3.00382 3.18698 3.09532 3.09563C3.27812 2.91278 3.27812 2.61633 3.09532 2.43348L2.05507 1.39313Z" fill="black" />
                                                    <path d="M2.40763 4.62975C2.40763 4.3712 2.19796 4.16162 1.93941 4.16162H0.468224C0.209718 4.16162 0 4.37115 0 4.62975C0 4.88826 0.209671 5.09793 0.468224 5.09793H1.93941C2.19796 5.09788 2.40763 4.88826 2.40763 4.62975Z" fill="black" />
                                                    <path d="M2.21797 6.0746L1.17758 7.11499C0.994681 7.29775 0.994681 7.5942 1.17758 7.77705C1.26898 7.86845 1.38871 7.91417 1.50863 7.91417C1.62841 7.91417 1.74819 7.86845 1.83959 7.77705L2.88002 6.73671C3.06287 6.5539 3.06287 6.2574 2.88002 6.0746C2.69727 5.8918 2.40086 5.8918 2.21797 6.0746Z" fill="black" />
                                                    <path d="M4.62951 2.40773C4.88801 2.40773 5.09768 2.19806 5.09768 1.9395V0.468177C5.09768 0.209671 4.88806 0 4.62951 0C4.37095 0 4.16138 0.209624 4.16138 0.468177V1.93955C4.16138 2.19806 4.37091 2.40773 4.62951 2.40773Z" fill="black" />
                                                    <path d="M6.40479 3.01714C6.52471 3.01714 6.64449 2.97151 6.73589 2.88007L7.77614 1.83977C7.95903 1.65692 7.95903 1.36047 7.77614 1.17766C7.59333 0.994815 7.29679 0.994815 7.11408 1.17766L6.07378 2.21796C5.89093 2.40076 5.89093 2.69726 6.07378 2.88007C6.16518 2.97151 6.28501 3.01714 6.40479 3.01714Z" fill="black" />
                                                </svg>
                                                <span className="m-l-5">20</span>
                                            </div>
                                        </Tippy>
                                        <div className="dot" />
                                        <Tippy content="Total Budget">
                                            <div className="budget flex ai-center">
                                                <svg width="16" height="16" viewbox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M8 14.5C11.866 14.5 14.5 11.866 14.5 8C14.5 4.13401 11.866 1.5 8 1.5C4.13401 1.5 1.5 4.13401 1.5 8C1.5 11.866 4.13401 14.5 8 14.5ZM8 16C12.4183 16 16 12.4183 16 8C16 3.58172 12.4183 0 8 0C3.58172 0 0 3.58172 0 8C0 12.4183 3.58172 16 8 16Z" fill="black" />
                                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M8.33329 4.33333C8.33329 4.14924 8.18405 4 7.99996 4C7.81586 4 7.66663 4.14924 7.66663 4.33333V5.33333H7.16663C6.7688 5.33333 6.38727 5.49136 6.10597 5.77267C5.82466 6.05397 5.66663 6.4355 5.66663 6.83333C5.66663 7.23115 5.82466 7.61268 6.10597 7.89399C6.38727 8.17529 6.7688 8.33333 7.16663 8.33333H7.66663V9.99999H5.99996C5.81586 9.99999 5.66663 10.1492 5.66663 10.3333C5.66663 10.5174 5.81586 10.6667 5.99996 10.6667H7.66663V11.6667C7.66663 11.8508 7.81586 12 7.99996 12C8.18405 12 8.33329 11.8508 8.33329 11.6667V10.6667H8.83329C9.23112 10.6667 9.61265 10.5086 9.89395 10.2273C10.1753 9.94602 10.3333 9.56449 10.3333 9.16666C10.3333 8.76884 10.1753 8.38731 9.89395 8.106C9.61265 7.8247 9.23112 7.66666 8.83329 7.66666H8.33329V5.99999H9.66663C9.85072 5.99999 9.99996 5.85076 9.99996 5.66666C9.99996 5.48257 9.85072 5.33333 9.66663 5.33333H8.33329V4.33333ZM7.66663 5.99999H7.16663C6.94561 5.99999 6.73365 6.08779 6.57737 6.24407C6.42109 6.40035 6.33329 6.61231 6.33329 6.83333C6.33329 7.05434 6.42109 7.2663 6.57737 7.42258C6.73365 7.57886 6.94561 7.66666 7.16663 7.66666H7.66663V5.99999ZM8.33329 8.33333V9.99999H8.83329C9.05431 9.99999 9.26627 9.9122 9.42255 9.75592C9.57883 9.59964 9.66663 9.38768 9.66663 9.16666C9.66663 8.94565 9.57883 8.73369 9.42255 8.57741C9.26627 8.42113 9.05431 8.33333 8.83329 8.33333H8.33329Z" fill="black" />
                                                </svg>
                                                <span className="m-l-5">$100</span>
                                            </div>
                                        </Tippy>
                                    </div>
                                </div>
                                <div class="preview-actions flex ai-center">
                                    <Link to="/edit-ad" className="flex">
                                        <FeatherIcon icon="edit-3" width={20} className="m-r-10" />
                                    </Link>
                                    <FeatherIcon icon="eye" width={20} />
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div className="preview-overlay"></div>
            </div>
        );
    }
}

export default ListingPreivew;