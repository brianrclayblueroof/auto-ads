import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import linksData from './data/adminlinksdata';

class AdminSideNav extends Component {
  constructor(props){
    super(props)
    this.state = {
      links: linksData.AdminSideNavLinks
    }
    this.handelLinkClick = this.handelLinkClick.bind(this)
  }

  handelLinkClick(name) {
    // console.log(this.state.whatever) if we use state bind in constructor
    // console.log(this.props.whatever) if we use props bind in contructor
    this.setState({
      links: this.state.links.map( link => {
        if(name === link.name){
          link.isActive = true;
        }else{
          link.isActive = false;
        }
        return link
      })
    })
  }

  render() {
    const links = this.state.links.map((link)=>{
      if(link.isInactive){
        return (
            <div className = "link inactive" key={link.name}>
              {link.img}
              <span>{link.name}</span>
            </div>
        )
      }else{
        return (
          <Link to={link.link} key={link.name}>
            <div className={ link.isActive ? `link active` : `link`} onClick={()=>{this.handelLinkClick(link.name)}}>
              {link.img}
              <span>{link.name}</span>
            </div>
          </Link>
        )
      }

    })
    return (
      <div className="side-nav admin">
        <div className="links">
          {links}
        </div>
      </div>
    );
  }
}

export default AdminSideNav;