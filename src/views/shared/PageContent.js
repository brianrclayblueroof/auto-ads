import React from 'react';
import { Switch, Route } from 'react-router-dom';
import AdminAgents from '../admin-agents/AdminAgents';
import AdminCampaigns from '../admin-campaigns/AdminCampaigns';
import AdminTemplates from '../admin-templates/AdminTemplates';
import AdminProfile from '../admin-profile/AdminProfile';
import AdminManageUsers from '../admin-manage-users/AdminManageUsers';

import Listings from '../listings/Listings';
import MyAds from '../my-ads/MyAds';
import NewAd from '../new-ad/NewAd';
import NewAdLocations from '../new-ad-locations/NewAdLocations';
import NewAdDemographics from '../new-ad-demographics/NewAdDemographics';
import NewAdBudget from '../new-ad-budget/NewAdBudget';
import NewAdContent from '../new-ad-content/NewAdContent';
import NewAdReview from '../new-ad-review/NewAdReview';

const PageContent = () => (
  <Switch>
    {/* Admin Routes */}
    <Route exact path='/' component={AdminAgents}/>
    <Route exact path='/admin-campaigns' component={AdminCampaigns}/>
    <Route exact path='/admin-templates' component={AdminTemplates}/>
    <Route exact path='/my-profile' component={AdminProfile}/>
    <Route exact path='/manage-users' component={AdminManageUsers}/>

    {/* Agent Routes */}
    <Route exact path='/listings' component={Listings}/>
    <Route exact path='/my-ads' component={MyAds}/>
    <Route exact path='/new-ad' component={NewAd}/>
    <Route exact path='/new-ad-locations' component={NewAdLocations}/>
    <Route exact path='/new-ad-demographics' component={NewAdDemographics}/>
    <Route exact path='/new-ad-budget' component={NewAdBudget}/>
    <Route exact path='/new-ad-content' component={NewAdContent}/>
    <Route exact path='/new-ad-review' component={NewAdReview}/>
  </Switch>
)

export default PageContent
