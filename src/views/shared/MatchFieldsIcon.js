import React, { Component } from 'react';


class MatchFieldsIcon extends Component {

    render() {

        return (
            <div className="match-field dropdown-trigger">
                <img src={require("../../assets/icons/match-field.svg")} />
            </div>
        );
    }
}

export default MatchFieldsIcon;