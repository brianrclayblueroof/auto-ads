import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import FeatherIcon from 'feather-icons-react';

const adStatuses = [
    {
        status: <span className="status-label">All Statuses</span>
    },
    {
        status:
                <div className="ad-status flex ai-center">
                    <div className="status running m-r-10" />
                    <span className="status-label">Running</span>
                </div>
    },
    {
        status:
                <div className="ad-status flex ai-center">
                    <div className="status paused m-r-10" />
                    <span className="status-label">Paused</span>
                </div>
    },
    {
        status:
                <div className="ad-status flex ai-center">
                    <div className="status pending m-r-10" />
                    <span className="status-label">Pending</span>
                </div>
    },
    {
        status:
                <div className="ad-status flex ai-center">
                    <div className="status error m-r-10" />
                    <span className="status-label">Error</span>
                </div>
    },
    {
        status:
                <div className="ad-status flex ai-center">
                    <div className="status ended m-r-10" />
                    <span className="status-label">Ended</span>
                </div>
    }
]
    
class AdStatusFilterMenu extends Component {
    constructor(props) {
        super(props)
        this.state = {
            adStatusEnabled: true
        }
    }

    toggleAdminUser(e) {
        console.log('hi')
    }

    render() {
        return (
            <ul className="dropdown-menu checkbox-menu">
                {adStatuses.map(adStatus => (
                    <li key={{adStatus}} className="dropdown-item checkbox-item" onClick={()=>{this.toggleAdminUser()}}>
                        <label className="checkbox">
                            <input type="checkbox" defaultChecked={this.state.adStatusEnabled} />
                            <span className="checkmark" />
                        </label>
                        { adStatus.status}
                    </li>
                ))}
            </ul>
        );
    }
}

export default AdStatusFilterMenu;