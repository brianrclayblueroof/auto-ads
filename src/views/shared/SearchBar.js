import React, { Component } from 'react';
import FeatherIcon from 'feather-icons-react'

class SearchBar extends Component {

    render() {

        return (
            <div className="input-wrapper m-r-10 w-30">
                <div className="input-btn search">
                    <input placeholder={this.props.placeholder ? this.props.placeholder : "Search"} />
                    <div>
                        <FeatherIcon icon={'search'}/>
                    </div>
                </div>
            </div>
        );
    }
}

export default SearchBar;