import React, { Component } from 'react';
import SearchBar from '../../../shared/SearchBar';
import AssignedToFilter from '../../../shared/assigned-to-filter/AssignedToFilter';
import AdStatusFilter from '../../../shared/ad-status-filter/AdStatusFilter';
import FiltersApplied from '../../../shared/FiltersApplied';

class AdminCampaignsToolbar extends Component {
    render() {
        return (
            <div className="panel flex jc-between ai-center tool-bar m-b-20">
                <div className="tool-bar-left flex ai-center flex1">
                    <SearchBar />
                    <AdStatusFilter />
                    <AssignedToFilter />
                    <FiltersApplied />
                </div>
                <div className="tool-bar-right">
                    {/* <button className="btn-primary">Add Agent</button> */}
                </div>
            </div>

        );
    }
}

export default AdminCampaignsToolbar;