import React, { Component } from 'react';
import TopNav from '../shared/top-nav/TopNav';
import AdminSideNav from '../shared/AdminSideNav';
import AdminCampaignsToolbar from './components/toolbar/AdminCampaignsToolbar';
import AdminCampaignsTable from './components/table/AdminCampaignsTable';

const AdminCampaigns = ()  => {
  return (
    <div>
      <TopNav />
      <AdminSideNav />
      <div className="page-content">
        <AdminCampaignsToolbar />
        <AdminCampaignsTable />
      </div>
    </div>
  );
}

export default AdminCampaigns;