import React, { Component } from 'react';
import TopNav from '../shared/top-nav/TopNav';
import SideNav from '../shared/SideNav';
import NewAdReviewToolbar from './components/NewAdReviewToolbar';
import NewAdReviewContent from './components/NewAdReviewContent';


class NewAdReview extends Component {
    render() {
        return (
            <div>
                <TopNav />
                <SideNav />
                <div className="page-content">
                    <NewAdReviewToolbar />
                    <NewAdReviewContent />
                </div>
            </div>
        );
    }
}

export default NewAdReview;