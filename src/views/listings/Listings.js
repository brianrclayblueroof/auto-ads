import React, { Component } from 'react';
import TopNav from '../shared/top-nav/TopNav';
import SideNav from '../shared/SideNav';
import ListingsToolbar from './components/toolbar/ListingsToolbar';
import ListingWrapper from './components/ListingWrapper';
import ListingPreivew from '../shared/listing-preview/ListingPreview';

class Listings extends Component {
    render() {
        return (
            <div>
                <TopNav />
                <SideNav />
                <div className="page-content">
                    <ListingsToolbar />
                    <ListingWrapper />
                </div>
                <ListingPreivew />
            </div>
        );
    }
}

export default Listings;