import React, { Component } from 'react';
import ListingCard from './listing-card/ListingCard';
import listingdata from '../../shared/data/listingdata';
import ListingImage from './listing-card/ListingImage';
import ListingActions from './listing-card/ListingActions';
import AdStatusTag from '../../shared/AdStatusTag';

class Listings extends Component {
    constructor(props){
        super(props)
        this.state = {
          listings: listingdata.listings
        }
    }
    render() {
        const listings = this.state.listings.map(listing=>{
            return <div className="listing">
                <ListingImage />
                <div className="listing-info flex ai-start jc-between m-t-20">
                    <div className="listing-address">
                        <h2>{listing.streetAddress}</h2>
                        <p>{listing.city}, {listing.state} {listing.zip}</p>
                    </div>
                    <AdStatusTag 
                        tagType={listing.adStatus}
                    />
                </div>
                <div className="listing-details flex ai-center m-t-15 m-b-20">
                    <span className="listing-price">{listing.price}</span>
                    <div className="dot" />
                    <span className="listing-beds">{listing.bedCount} bd</span>
                    <div className="dot" />
                    <span className="listing-baths">{listing.bathCount} ba</span>
                    <div className="dot" />
                    <span className="listing-status">{listing.status}</span>
                </div>
                <ListingActions />
            </div>
        })

        return (
            <div className="listings-container m-b-40">
                {listings}
            </div>
        );
    }
}

export default Listings;