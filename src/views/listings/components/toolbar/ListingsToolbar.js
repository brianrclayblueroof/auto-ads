import React, { Component } from 'react';
import SearchBar from '../../../shared/SearchBar';
import AdStatusFilter from '../../../shared/ad-status-filter/AdStatusFilter';
import FiltersApplied from '../../../shared/FiltersApplied';
import ListingStatusFilter from '../../../shared/listing-status-filter/ListingStatusFilter';

class ListingsToolbar extends Component {
    render() {
        return (
            <div className="panel flex jc-between ai-center tool-bar m-b-20">
                <div className="tool-bar-left flex ai-center flex1">
                    <SearchBar />
                    <AdStatusFilter />
                    <ListingStatusFilter />
                    <FiltersApplied />
                </div>
                <div className="tool-bar-right">
                    {/* <button className="btn-primary">Add Listing</button> */}
                </div>
            </div>
        );
    }
}

export default ListingsToolbar;