import React, { Component } from 'react';
import FeatherIcon from 'feather-icons-react';

class ListingActions extends Component {
    render() {
        return (
            <div className="listing-actions flex ai-center">
                <div className="listing-manage-ads grey flex jc-center ai-center pointer">
                    <FeatherIcon icon={'edit-2'} width={20} />
                    <span className="m-l-10">Manage Ads</span>
                </div>
            </div>
        );
    }
}

export default ListingActions;