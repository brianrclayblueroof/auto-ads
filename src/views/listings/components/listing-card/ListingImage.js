import React, { Component } from 'react';

class ListingImage extends Component {
    render() {
        return (
            <div className="listing-img" style={{ backgroundImage: "url(" + require('../../../../assets/images/placeholder/listing-default.png') + ")" }}>
                <div className="listing-select">
                    <img alt="" src={require("../../../../assets/icons/check-white.svg")} />
                </div>
            </div>
        );
    }
}

export default ListingImage;