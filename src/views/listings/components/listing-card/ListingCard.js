import React, { Component } from 'react';
import ListingImage from './ListingImage';
import ListingActions from './ListingActions';
import ListingInfo from './ListingInfo';

// class ListingCard extends Component {
//     render() {
    const ListingCard = (props) => {
        return (
            <div className="listing">
                <ListingImage />
                <div className="listing-info flex ai-start jc-between m-t-20">
                    <div className="listing-address">
                        <h2>{this.props.streetAddress}</h2>
                        <p>{this.props.city}, {this.props.state} {this.props.zip}</p>
                    </div>
                    <div className="ad-status-tag running">Running</div>
                </div>
                <div className="listing-details flex ai-center m-t-15 m-b-20">
                    <span className="listing-price">{this.props.price}</span>
                    <div className="dot" />
                    <span className="listing-beds">{this.props.bedCount} bd</span>
                    <div className="dot" />
                    <span className="listing-baths">{this.props.bathCount} ba</span>
                    <div className="dot" />
                    <span className="listing-status">{this.props.status}</span>
                </div>
                <ListingActions />
            </div>
        );
    }
// }

export default ListingCard;