import React, { Component } from 'react';

class AdTypeMenu extends Component {

    render() {
        return (
            <ul className="dropdown-menu">
                <li className="dropdown-item">
                    <label className="radio">
                        Single Listing
                        <input type="radio" name="ad-type" defaultChecked="true" />
                        <span className="radio-indicator" />
                    </label>
                </li>
                <li className="dropdown-item">
                    <label className="radio inactive">
                        Multiple Listings
                        <input type="radio" name="ad-type" />
                        <span className="radio-indicator" />
                    </label>
                </li>
            </ul>
        );
    }
}

export default AdTypeMenu;