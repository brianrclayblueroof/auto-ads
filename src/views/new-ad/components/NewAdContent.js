import React, { Component } from 'react';
import AdType from './AdType';
import Tippy from '@tippy.js/react';
import FeatherIcon from 'feather-icons-react';

class NewAdContent extends Component {
    render() {
        return (
            <div className="panel">
                <div className="input-wrapper w-25">
                    <div className="help-label">
                        <label>Ad Name</label>
                    </div>
                    <input placeholder="Enter ad name" />
                </div>
                <div className="input-group w-50 m-t-30 m-b-10">
                    <div className="input-wrapper">
                        <div className="help-label">
                            <label>Ad Type</label>
                            <Tippy content="Select single listing to feature one property or multiple listings to feature up to 10 listings.">
                                <FeatherIcon icon="info" className="blue m-l-5" width={16} />
                            </Tippy>
                        </div>
                        <AdType />
                    </div>
                    <div className="input-wrapper">
                        <div className="help-label">
                            <label>MLS ID#</label>
                            <Tippy content="Enter an MLS ID# to pull in data from a specific listing">
                                <FeatherIcon icon="info" className="blue m-l-5" width={16} />
                            </Tippy>
                        </div>
                        <input placeholder="Enter MLS# (optional)" />
                    </div>
                </div>
            </div>
        );
    }
}

export default NewAdContent;