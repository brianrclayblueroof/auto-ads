import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class NewAdToolbar extends Component {
    render() {
        return (
            <div className="panel flex jc-between ai-center tool-bar m-b-20">
                <div className="tool-bar-left flex ai-center flex1">
                    <h2>Create an Ad</h2>
                </div>
                <div className="tool-bar-right">
                    <div className="btn-group">
                        <Link to="/my-ads">
                            <div className="btn btn-cancel">Cancel</div>
                        </Link>
                        <Link to="/new-ad-locations">
                            <div className="btn btn-primary">Continue</div>
                        </Link>
                    </div>
                </div>
            </div>

        );
    }
}

export default NewAdToolbar;