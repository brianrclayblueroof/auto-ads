import React, { Component } from 'react';
import TopNav from '../shared/top-nav/TopNav';
import SideNav from '../shared/SideNav';
import NewAdToolbar from './components/NewAdToolbar';
import NewAdContent from './components/NewAdContent';

class NewAd extends Component {
    render() {
        return (
            <div>
                <TopNav />
                <SideNav />
                <div className="page-content">
                    <NewAdToolbar />
                    <NewAdContent />
                </div>
            </div>
        );
    }
}

export default NewAd;