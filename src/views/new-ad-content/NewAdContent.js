import React, { Component } from 'react';
import TopNav from '../shared/top-nav/TopNav';
import SideNav from '../shared/SideNav';
import NewAdContentToolbar from './components/NewAdContentToolbar';
import NewAdContentSettings from './components/NewAdContentSettings';


class NewAdContent extends Component {
    render() {
        return (
            <div>
                <TopNav />
                <SideNav />
                <div className="page-content">
                    <NewAdContentToolbar />
                    <NewAdContentSettings />
                </div>
            </div>
        );
    }
}

export default NewAdContent;