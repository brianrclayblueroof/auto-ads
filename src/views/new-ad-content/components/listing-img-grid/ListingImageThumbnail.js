import React, { Component, Fragment } from 'react';

class ListingImageThumbnail extends Component {
    render() {
        return (
            <div className={this.props.selected ? `card-thumbnail selected` : `card-thumbnail`} style ={ { backgroundImage: "url("+require('../../../../assets/images/placeholder/listing-default.png')+")" } } />
        );
    }
}

export default ListingImageThumbnail;