import React, { Component, Fragment } from 'react';
import ListingImageThumbnail from './ListingImageThumbnail'

class ListingImageGrid extends Component {
    render() {
        return (
            <div className="listing-img-grid">
                <ListingImageThumbnail selected/>
                <ListingImageThumbnail />
                <ListingImageThumbnail />
                <ListingImageThumbnail />
                <ListingImageThumbnail />
                <ListingImageThumbnail />
                <ListingImageThumbnail />
                <ListingImageThumbnail />
            </div>
        );
    }
}

export default ListingImageGrid;