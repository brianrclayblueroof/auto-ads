import React, { Component } from 'react';
import Tippy from '@tippy.js/react';
import FeatherIcon from 'feather-icons-react';
import FacebookAccountPicker from '../../shared/fb-account-picker/FacebookAccountPicker';
import FacebookPixelToggle from '../../shared/fb-pixel-toggle/FacebookPixelToggle';

class FormatSelector extends Component {
    render() {
        return (
            <div className="format-wrapper">
                <div className="format">
                    <label className="radio format-select">
                        <input type="radio" name="format" />
                        <span className="radio-indicator" />
                    </label>
                    <img src={require("../../../assets/images/ad-format/single-image.png")} />
                    <h3>Single Image</h3>
                    <p className="grey">Create an ad with one image</p>
                </div>
                <div className="format active">
                    <label className="radio format-select">
                        <input type="radio" name="format" defaultChecked />
                        <span className="radio-indicator" />
                    </label>
                    <img src={require("../../../assets/images/ad-format/carousel-active.png")} />
                    <h3>Carousel</h3>
                    <p className="grey">Create an ad with 2 or more scrollable images</p>
                </div>
                <div className="format inactive">
                    <label className="radio format-select">
                        <input type="radio" name="format" />
                        <span className="radio-indicator" />
                    </label>
                    <img src={require("../../../assets/images/ad-format/single-video.png")} />
                    <h3>Single Video</h3>
                    <p className="grey">Create an ad with one video or turn images into a video</p>
                </div>
                <div className="format inactive">
                    <label className="radio format-select">
                        <input type="radio" name="format" />
                        <span className="radio-indicator" />
                    </label>
                    <img src={require("../../../assets/images/ad-format/collection.png")} />
                    <h3>Collection</h3>
                    <p className="grey">Create a looping video ad with up to 10 images</p>
                </div>
            </div>
        );
    }
}

export default FormatSelector;