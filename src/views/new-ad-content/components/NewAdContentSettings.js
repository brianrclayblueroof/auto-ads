import React, { Component } from 'react';
import Tippy from '@tippy.js/react';
import FeatherIcon from 'feather-icons-react';
import FacebookAccountPicker from '../../shared/fb-account-picker/FacebookAccountPicker';
import FacebookPixelToggle from '../../shared/fb-pixel-toggle/FacebookPixelToggle';
import FormatSelector from './FormatSelector';
import FacebookAdPreview from '../../shared/fb-ad-preview/FacebookAdPreview';
import AdContentText from '../../shared/AdContentText';
import CarouselCardSettings from './CarouselCardSettings';

class NewAdContentSettings extends Component {
    render() {
        return (
            <div className="flex ai-start m-b-40">
                <div className="col flex1">
                    <div className="panel">
                        <div className="input-group w-50">
                            <div className="input-wrapper">
                                <div className="help-label">
                                    <label>Facebook Page</label>
                                    <Tippy content="Your Facebook Page or Instagram account represents your business in ads.">
                                        <FeatherIcon icon="info" className="blue m-l-5" width={16} />
                                    </Tippy>
                                </div>
                                <FacebookAccountPicker />
                            </div>
                            <div className="input-wrapper">
                                <div className="help-label">
                                    <label>Facebook Pixel</label>
                                    <Tippy content="Add pixel code to your website and Instant Experience to report conversions, see activity and build audiences for ad targeting.">
                                        <FeatherIcon icon="info" className="blue m-l-5" width={16} />
                                    </Tippy>
                                </div>
                                <FacebookPixelToggle />
                            </div>
                        </div>
                        <div className="help-label m-t-30">
                            <label>Format</label>
                            <Tippy content="Choose how you'd like your ad to look.">
                                <FeatherIcon icon="info" className="blue m-l-5" width={16} />
                            </Tippy>
                        </div>
                        <FormatSelector />
                        <div className="m-t-30 w-50">
                            <AdContentText />
                        </div>
                        <CarouselCardSettings />
                    </div>
                </div>
                <div className="col flex0 fb-ad-preview-wrapper">
                    <FacebookAdPreview />
                </div>
            </div>

        );
    }
}

export default NewAdContentSettings;