import React, { Component } from 'react';

class ImageTypeMenu extends Component {

    render() {
        return (
            <ul className="dropdown-menu checkbox-menu">
                <li className="dropdown-item">
                    <label className="radio">
                        <div className="flex ai-center">
                            Listing Images
                        </div>
                        <input type="radio" name="image-type" defaultChecked="true" />
                        <span className="radio-indicator" />
                    </label>
                </li>
                <li className="dropdown-item">
                    <label className="radio">
                        <div className="flex ai-center">
                            My Images
                        </div>
                        <input type="radio" name="image-type" />
                        <span className="radio-indicator" />
                    </label>
                </li>
            </ul>
        );
    }
}

export default ImageTypeMenu;