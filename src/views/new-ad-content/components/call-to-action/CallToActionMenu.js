import React, { Component } from 'react';

class CallToActionMenu extends Component {

    render() {
        return (
            <ul className="dropdown-menu checkbox-menu">
                <li className="dropdown-item">
                    <label className="radio">
                        <div className="flex ai-center">
                            Learn More
                        </div>
                        <input type="radio" name="call-to-action" defaultChecked/>
                        <span className="radio-indicator" />
                    </label>
                </li>
                <li className="dropdown-item">
                    <label className="radio">
                        <div className="flex ai-center">
                            Sign Up
                        </div>
                        <input type="radio" name="call-to-action" />
                        <span className="radio-indicator" />
                    </label>
                </li>
                <li className="dropdown-item">
                    <label className="radio">
                        <div className="flex ai-center">
                            Subscribe
                        </div>
                        <input type="radio" name="call-to-action"/>
                        <span className="radio-indicator" />
                    </label>
                </li>
                <li className="dropdown-item">
                    <label className="radio">
                        <div className="flex ai-center">
                            Watch More
                        </div>
                        <input type="radio" name="call-to-action" />
                        <span className="radio-indicator" />
                    </label>
                </li>
                <li className="dropdown-item">
                    <label className="radio">
                        <div className="flex ai-center">
                            Apply Now
                        </div>
                        <input type="radio" name="call-to-action"/>
                        <span className="radio-indicator" />
                    </label>
                </li>
                <li className="dropdown-item">
                    <label className="radio">
                        <div className="flex ai-center">
                            Book Now
                        </div>
                        <input type="radio" name="call-to-action" />
                        <span className="radio-indicator" />
                    </label>
                </li>
                <li className="dropdown-item">
                    <label className="radio">
                        <div className="flex ai-center">
                            Contact Us
                        </div>
                        <input type="radio" name="call-to-action"/>
                        <span className="radio-indicator" />
                    </label>
                </li>
                <li className="dropdown-item">
                    <label className="radio">
                        <div className="flex ai-center">
                            Donate Now
                        </div>
                        <input type="radio" name="call-to-action" />
                        <span className="radio-indicator" />
                    </label>
                </li>
                <li className="dropdown-item">
                    <label className="radio">
                        <div className="flex ai-center">
                            Download
                        </div>
                        <input type="radio" name="call-to-action"/>
                        <span className="radio-indicator" />
                    </label>
                </li>
                <li className="dropdown-item">
                    <label className="radio">
                        <div className="flex ai-center">
                            Get Offer
                        </div>
                        <input type="radio" name="call-to-action" />
                        <span className="radio-indicator" />
                    </label>
                </li>
                <li className="dropdown-item">
                    <label className="radio">
                        <div className="flex ai-center">
                            Get Quote
                        </div>
                        <input type="radio" name="call-to-action" />
                        <span className="radio-indicator" />
                    </label>
                </li>
                <li className="dropdown-item">
                    <label className="radio">
                        <div className="flex ai-center">
                            Get Showtimes
                        </div>
                        <input type="radio" name="call-to-action"/>
                        <span className="radio-indicator" />
                    </label>
                </li>
                <li className="dropdown-item">
                    <label className="radio">
                        <div className="flex ai-center">
                            Listen Now
                        </div>
                        <input type="radio" name="call-to-action" />
                        <span className="radio-indicator" />
                    </label>
                </li>
                <li className="dropdown-item">
                    <label className="radio">
                        <div className="flex ai-center">
                            Request Time
                        </div>
                        <input type="radio" name="call-to-action" />
                        <span className="radio-indicator" />
                    </label>
                </li>
                <li className="dropdown-item">
                    <label className="radio">
                        <div className="flex ai-center">
                            See Menu
                        </div>
                        <input type="radio" name="call-to-action" />
                        <span className="radio-indicator" />
                    </label>
                </li>
                <li className="dropdown-item">
                    <label className="radio">
                        <div className="flex ai-center">
                            Shop Now
                        </div>
                        <input type="radio" name="call-to-action" />
                        <span className="radio-indicator" />
                    </label>
                </li>
            </ul>
        );
    }
}

export default CallToActionMenu;