import React, { Component, Fragment } from 'react';
import Tippy from '@tippy.js/react';
import FeatherIcon from 'feather-icons-react';
import Button from '../../shared/Button'
import ImageType from './image-type/ImageType';
import ListingImageGrid from './listing-img-grid/ListingImageGrid';
import MatchFieldLabel from '../../shared/MatchFieldLabel';
import CallToAction from './call-to-action/CallToAction';

class CarouselCardSettings extends Component {
    render() {
        return (
            <Fragment>
                <div className="help-label m-t-30">
                    <label>Cards</label>
                    <Tippy content="Select the minimum and maximum age of the people who will find your ad relevant.">
                        <FeatherIcon icon="info" className="blue m-l-5" width={16} />
                    </Tippy>
                </div>
                <div className="flex ai-center">
                    <div className="segment-btn">
                        {/* <Button buttonText="1" className="active" />
                        <Button buttonText="2" />
                        <Button buttonText="3" /> */}
                        <div className="btn active card-btn">
                            <span>1</span>
                            <div className="card-carret"></div>
                        </div>
                        <div className="btn card-btn">
                            <span>2</span>
                            <div className="card-carret"></div>
                        </div>
                    </div>
                    <Tippy content="Add Card">
                        <Button buttonText={<FeatherIcon icon="plus" />} className="m-l-15" />
                    </Tippy>
                </div>
                <div className="card-content">
                    <div className="card-img-wrapper flex w-100">
                        <div className="col">
                            <div className="help-label m-t-0">
                                <label>Image</label>
                                <Tippy content="To maximize ad delivery, use an image that contains little or no overlaid text.">
                                    <FeatherIcon icon="info" className="blue m-l-5" width={16} />
                                </Tippy>
                            </div>
                            <div className="card-img m-b-30" style={{ backgroundImage: "url(" + require('../../../assets/images/placeholder/listing-default.png') + ")" }} />
                        </div>
                        <div className="col">
                            <div className="flex jc-between ai-center">
                                <ImageType />
                                <span className="blue pointer">Upload Image</span>
                            </div>
                            <ListingImageGrid />
                        </div>
                    </div>
                    <div className="card-details-wrapper flex">
                        <div className="col">
                            <MatchFieldLabel
                                label="Headline"
                                helpText="Add a headline that grabs people's attention and tells them what your ad is about. The character limit for headline is 40. You can add more text and go over the character limit, but it may be cut off when people see your ad in certain placements, for example Mobile News Feed."
                            />
                            <div class="input-wrapper w-100 m-b-30">
                                <div className="input-counter">
                                    <input placeholder="Add an attention grabbing headline" maxLength={40} />
                                    <div className="char-counter">
                                        <span className="current-count">0</span>
                                        <span>/</span>
                                        <span className="max-count">40</span>
                                    </div>
                                </div>
                            </div>
                            <MatchFieldLabel
                                label="Destination URL"
                                helpText="Add a headline that grabs people's attention and tells them what your ad is about. The character limit for headline is 40. You can add more text and go over the character limit, but it may be cut off when people see your ad in certain placements, for example Mobile News Feed."
                            />
                            <div className="input-wrapper w-100">
                                <div className="input-btn">
                                    <input placeholder="Enter the URL you want to promote" />
                                    <a href="#">
                                        <FeatherIcon icon="external-link" />
                                    </a>
                                </div>
                            </div>

                        </div>
                        <div className="col">
                            <MatchFieldLabel
                                label="Description"
                                helpText="Enter the destination URL that you want to promote. Ex: http://www.example.com/page"
                            />
                            <div class="input-wrapper w-100">
                                <div className="input-counter">
                                    <input placeholder="Enter a brief description of what you're promoting" maxLength={40} />
                                    <div className="char-counter">
                                        <span className="current-count">0</span>
                                        <span>/</span>
                                        <span className="max-count">40</span>
                                    </div>
                                </div>
                            </div>
                            <div className="help-label m-t-30">
                                <label>Call to Action</label>
                                <Tippy content="Choose the action you want people to take when they see your ad.">
                                    <FeatherIcon icon="info" className="blue m-l-5" width={16} />
                                </Tippy>
                            </div>
                            <CallToAction />
                        </div>
                    </div>
                </div>
            </Fragment>
        );
    }
}

export default CarouselCardSettings;