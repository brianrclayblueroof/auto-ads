import React, { Component } from 'react';

class BudgetTypeMenu extends Component {

    render() {
        return (
            <ul className="dropdown-menu">
                <li className="dropdown-item">
                    <label className="radio">
                        Daily Budget
                        <input type="radio" name="include-exclude" defaultChecked="true" />
                        <span className="radio-indicator" />
                    </label>
                </li>
                <li className="dropdown-item">
                    <label className="radio">
                        Total Budget
                        <input type="radio" name="include-exclude" />
                        <span className="radio-indicator" />
                    </label>
                </li>
            </ul>
        );
    }
}

export default BudgetTypeMenu;