import React, { Component } from 'react';
import Tippy from '@tippy.js/react';
import FeatherIcon from 'feather-icons-react';
import BudgetType from './BudgetType';

class NewAdBudgetContent extends Component {
    render() {
        return (
            <div className="panel">
                <div className="help-label">
                    <label>Schedule</label>
                    <img src="/assets/icons/info-blue.svg" data-tippy="Set how long you want this ad to run" />
                </div>
                <label className="radio m-b-10">
                    Run ad continuosly starting today
                    <input type="radio" name="duration" />
                    <span className="radio-indicator" />
                </label>
                <label className="radio m-b-15">
                    Set a start and end date
                    <input type="radio" defaultChecked="checked" name="duration" />
                    <span className="radio-indicator" />
                </label>
                <div className="start-stop-picker callout">
                    <div className="flex ai-center m-b-10">
                        <span className="m-r-10 picker-label">Start</span>
                        <div className="input-wrapper m-r-5">
                            <div className="input-btn">
                                <input defaultValue="Nov 2, 2018" />
                                <div className="bg-lightest-grey">
                                    <FeatherIcon icon="calendar" />
                                </div>
                            </div>
                        </div>
                        <div className="input-wrapper">
                            <div className="input-btn">
                                <input defaultValue="11:10" />
                                <div className="bg-lightest-grey">
                                    <FeatherIcon icon="clock" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="flex ai-center">
                        <span className="m-r-10 picker-label">End</span>
                        <div className="input-wrapper m-r-5">
                            <div className="input-btn">
                                <input defaultValue="Nov 3, 2018" />
                                <div className="bg-lightest-grey">
                                    <FeatherIcon icon="calendar" />
                                </div>
                            </div>
                        </div>
                        <div className="input-wrapper">
                            <div className="input-btn">
                                <input defaultValue="11:10" />
                                <div className="bg-lightest-grey">
                                    <FeatherIcon icon="clock" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="help-label m-t-30">
                    <label>Budget</label>
                    <img src="/assets/icons/info-blue.svg" data-tippy="Set how much you want to pay for this ad" />
                </div>
                <div className="inline-flex ai-center">
                    <BudgetType />
                    <div className="input-wrapper w-100 flex1 m-l-5">
                        <input placeholder="Daily Budget" defaultValue="$10.00" className="outline-input" />
                    </div>
                </div>
                <div className="callout primary m-t-30">
                    This ad will run for <strong>3</strong> days. You’ll spend no more than <strong>$100.83</strong>.
                </div>
            </div>

        );
    }
}

export default NewAdBudgetContent;