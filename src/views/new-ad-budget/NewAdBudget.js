import React, { Component } from 'react';
import TopNav from '../shared/top-nav/TopNav';
import SideNav from '../shared/SideNav';
import NewAdBudgetToolbar from './components/NewAdBudgetToolbar';
import NewAdBudgetContent from './components/NewAdBudgetContent';


class NewAdBudget extends Component {
    render() {
        return (
            <div>
                <TopNav />
                <SideNav />
                <div className="page-content">
                    <NewAdBudgetToolbar />
                    <NewAdBudgetContent />
                </div>
            </div>
        );
    }
}

export default NewAdBudget;