import React, { Component } from 'react';
import TopNav from '../shared/top-nav/TopNav';
import SideNav from '../shared/SideNav';
import NewAdDemographicsToolbar from './components/NewAdDemographicsToolbar';
import NewAdDemographicsContent from './components/NewAdDemographicsContent';


class NewAdDemographics extends Component {
    render() {
        return (
            <div>
                <TopNav />
                <SideNav />
                <div className="page-content">
                    <NewAdDemographicsToolbar />
                    <NewAdDemographicsContent />
                </div>
            </div>
        );
    }
}

export default NewAdDemographics;