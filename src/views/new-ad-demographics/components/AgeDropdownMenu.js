import React, { Component } from 'react';

class AgeDropdownMenu extends Component {

    render() {
        return (
            <ul className="dropdown-menu">
                <li className="dropdown-item">
                    <label className="radio">
                        18
                        <input type="radio" name="include-exclude" defaultChecked="true" />
                        <span className="radio-indicator" />
                    </label>
                </li>
                <li className="dropdown-item">
                    <label className="radio">
                        19
                        <input type="radio" name="include-exclude" />
                        <span className="radio-indicator" />
                    </label>
                </li>
                <li className="dropdown-item">
                    <label className="radio">
                        20
                        <input type="radio" name="include-exclude" defaultChecked="true" />
                        <span className="radio-indicator" />
                    </label>
                </li>
                <li className="dropdown-item">
                    <label className="radio">
                        21
                        <input type="radio" name="include-exclude" />
                        <span className="radio-indicator" />
                    </label>
                </li>
            </ul>
        );
    }
}

export default AgeDropdownMenu;