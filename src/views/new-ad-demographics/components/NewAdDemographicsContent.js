import React, { Component } from 'react';
import Tippy from '@tippy.js/react';
import FeatherIcon from 'feather-icons-react';
import AgeDropdown from './AgeDropdown';
import Button from '../../shared/Button'

class NewAdDemographicsContent extends Component {
    render() {
        return (
            <div className="panel">
                <div className="input-group w-50">
                    <div className="input-wrapper">
                        <div className="help-label">
                            <label>Age</label>
                            <img src="/assets/icons/info-blue.svg" data-tippy="Age of people you want to see this ad." />
                        </div>
                        <div className="flex ai-center">
                            <AgeDropdown 
                                triggerLabel="18"
                            />
                            <div className="age-separator" />
                            <AgeDropdown 
                                triggerLabel="65+"
                            />
                        </div>
                    </div>
                    <div className="input-wrapper">
                        <div className="help-label">
                            <label>Gender</label>
                        </div>
                        <div className="segment-btn">
                            <Button buttonText="All" className="active"/>
                            <Button buttonText="Men" />
                            <Button buttonText="Women" />
                        </div>
                    </div>
                </div>
                <div className="input-wrapper w-25 m-t-30 m-b-10">
                    <label>Languages</label>
                    <div className="input-btn search">
                        <input placeholder="Add a language" />
                        <div>
                            <FeatherIcon icon={'plus-circle'}/>
                        </div>
                    </div>
                </div>
            </div>

        );
    }
}

export default NewAdDemographicsContent;