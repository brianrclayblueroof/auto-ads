import React from 'react';
import Modal from 'react-modal';
import FeatherIcon from 'feather-icons-react';

class EditProfileName extends React.Component {

    render() {
        return (
            <Modal
                isOpen={this.props.isOpen}
                className={this.props.className}
                onRequestClose={this.props.modalClosed}
                shouldCloseOnOverlayClick={true}
            >
                <div className="popup-header">
                    <h2>Edit Name</h2>
                    <div className="close-popup" onClick={this.props.modalClosed}>
                        <FeatherIcon icon={"x"} />
                    </div>
                </div>
                <div class="input-group">
                    <div className="input-wrapper">
                        <label>Frist Name</label>
                        <input placeholder="Enter First Name" value="Brian" />
                    </div>
                    <div className="input-wrapper">
                        <label>Last Name</label>
                        <input placeholder="Enter Last Name" value="Clay" />
                    </div>
                </div>

                <div className="popup-buttons">
                    <button className="close-popup btn-cancel" onClick={this.props.modalClosed}>Cancel</button>
                    <button className="close-popup btn-primary" onClick={this.props.modalClosed}>Save</button>
                </div>

            </Modal>
        );
    }
}

export default EditProfileName;