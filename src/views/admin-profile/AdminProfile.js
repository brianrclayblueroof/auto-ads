import React, { Component } from 'react';
import TopNav from '../shared/top-nav/TopNav';
import AdminSideNav from '../shared/AdminSideNav';
import FeatherIcon from 'feather-icons-react';
import Tippy from '@tippy.js/react';
import EditProfileName from './popups/EditProfileName'


class AdminProfile extends Component {
  constructor(props){
    super(props)
    this.state = {
      editProfileNameOpen: false
    }
    this.openModal = this.openModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
  }

  openModal() {
    this.setState({editProfileNameOpen: true});
  }

  closeModal() {
    this.setState({editProfileNameOpen: false});
  }

  render() {
    return (
      <div>
        <TopNav />
        <AdminSideNav />
        <div className="page-content flex ai-start constrain">
          <div className="col flex1">
            <div className="panel p-30">
              <div className="profile-pic" style ={ { backgroundImage: "url("+require('../../assets/images/placeholder/profile-img.png')+")" } }>
                <div className="upload-profile-pic white">
                  <FeatherIcon icon={'upload-cloud'} />
                  <span>Upload</span>
                </div>
              </div>
              <Tippy content={"Click to edit name"}>
                <h1 className="ta-center pointer" onClick={this.openModal}>Brian Clay</h1>
              </Tippy>
              <ul className="profile-info m-t-30">
                <li className="flex ai-center jc-between">
                  <span>Username</span>
                  <div className="flex ai-center jc-end">
                    <span className="info">brian.c</span>
                    <input className="edit-info hide" maxLength={25} defaultValue="brian.c" />
                    <FeatherIcon icon={'edit-3'} width={18} className={"m-l-15 pointer grey"} />
                  </div>
                </li>
                <li className="flex ai-center jc-between">
                  <span>Email</span>
                  <div className="flex ai-center jc-end">
                    <span className="info">brian.c@gmail.com</span>
                    <input className="edit-info hide" defaultValue="brian.c@gmail.com" />
                    <FeatherIcon icon={'edit-3'} width={18} className={"m-l-15 pointer grey"} />
                  </div>
                </li>
                <li className="flex ai-center jc-between">
                  <span>Phone</span>
                  <div className="flex ai-center jc-end">
                    <span className="info">919-710-2117</span>
                    <input className="edit-info hide" defaultValue="919-710-2117" />
                    <FeatherIcon icon={'edit-3'} width={18} className={"m-l-15 pointer grey"} />
                  </div>
                </li>
                <li className="flex ai-center jc-between">
                  <span>Permissions</span>
                  <div className="flex ai-center jc-end">
                    <span data-tippy="Only owners can edit permissions">Ads, Posts, Templates</span>
                  </div>
                </li>
                <li>
                  <p className="blue pointer">Reset Password</p>
                </li>
                <li>
                  <p className="blue pointer">Logout</p>
                </li>
              </ul>
            </div>
          </div>
          <div className="col flex2">
            <div className="panel p-30">
              <div className="flex ai-center jc-between m-b-30">
                <h2>Notification</h2>
                <p className="blue pointer">Turn off all notifications</p>
              </div>
              <table id="notifications-table">
                <thead>
                  <tr>
                    <th />
                    <th className="ta-center">Email</th>
                    <th className="inactive ta-center">Text</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>Auto Ad Runs
              <img alt="" className="pointer m-l-5" src="/assets/icons/info-blue.svg" data-tippy="Get notified anytime an auto ad runs for a listing or group of listings" width="18px" height="18px" />
                    </td>
                    <td className="ta-center">
                      <label className="checkbox">
                        <input type="checkbox" defaultChecked="checked" />
                        <span className="checkmark" />
                      </label>
                    </td>
                    <td className="ta-center">
                      <label className="checkbox">
                        <input type="checkbox" />
                        <span className="checkmark" />
                      </label>
                    </td>
                  </tr>
                  <tr>
                    <td>Ad Ends
              <img alt="" className="pointer m-l-5" src="/assets/icons/info-blue.svg" data-tippy="Get notified when an ad ends" width="18px" height="18px" />
                    </td>
                    <td className="ta-center">
                      <label className="checkbox">
                        <input type="checkbox" defaultChecked="checked" />
                        <span className="checkmark" />
                      </label>
                    </td>
                    <td className="ta-center">
                      <label className="checkbox">
                        <input type="checkbox" />
                        <span className="checkmark" />
                      </label>
                    </td>
                  </tr>
                  <tr>
                    <td>Ad CPC Spikes or Dips
              <img alt="" className="pointer m-l-5" src="/assets/icons/info-blue.svg" data-tippy="Get notified anytime an ad's CPC increases or decreses more than 15% in a 24 hour period." width="18px" height="18px" />
                    </td>
                    <td className="ta-center">
                      <label className="checkbox">
                        <input type="checkbox" defaultChecked="checked" />
                        <span className="checkmark" />
                      </label>
                    </td>
                    <td className="ta-center">
                      <label className="checkbox">
                        <input type="checkbox" />
                        <span className="checkmark" />
                      </label>
                    </td>
                  </tr>
                  <tr>
                    <td>Ad Clicks Spike or Dip
              <img alt="" className="pointer m-l-5" src="/assets/icons/info-blue.svg" data-tippy="Get notified anytime an ad's number of click increases or decreses more than 15% in a 24 hour period." width="18px" height="18px" />
                    </td>
                    <td className="ta-center">
                      <label className="checkbox">
                        <input type="checkbox" defaultChecked="checked" />
                        <span className="checkmark" />
                      </label>
                    </td>
                    <td className="ta-center">
                      <label className="checkbox">
                        <input type="checkbox" />
                        <span className="checkmark" />
                      </label>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <EditProfileName 
          isOpen = {this.state.editProfileNameOpen}
          modalClosed = {this.closeModal}
        />
      </div>
    );
  }
}

export default AdminProfile;