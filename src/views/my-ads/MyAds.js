import React, { Component } from 'react';
import TopNav from '../shared/top-nav/TopNav';
import SideNav from '../shared/SideNav';
import MyAdsToolbar from './components/MyAdsToolbar';
import MyAdsTable from './components/MyAdsTable';

class MyAds extends Component {
    render() {
        return (
            <div>
                <TopNav />
                <SideNav />
                <div className="page-content">
                    <MyAdsToolbar />
                    <MyAdsTable />
                </div>
            </div>
        );
    }
}

export default MyAds;