import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import AdStatusFilter from '../../shared/ad-status-filter/AdStatusFilter';
import FiltersApplied from '../../shared/FiltersApplied';
import FeatherIcon from 'feather-icons-react';

class MyAdsToolbar extends Component {
    render() {
        return (
            <div className="panel flex jc-between ai-center tool-bar m-b-20">
                <div className="tool-bar-left flex ai-center flex1">
                    <div className="input-wrapper m-r-10 w-30">
                        <div className="input-btn search">
                            <input placeholder="Search" />
                            <div>
                                <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} viewbox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth={2} strokeLinecap="round" strokeLinejoin="round" className="feather feather-search"><circle cx={11} cy={11} r={8} /><line x1={21} y1={21} x2="16.65" y2="16.65" /></svg>
                            </div>
                        </div>
                        <div className="input-message">
                            <img alt="" src="/assets/icons/check-circle-green.svg" />
                            <span>This is a success message</span>
                        </div>
                    </div>
                    <AdStatusFilter />
                    <FiltersApplied />
                </div>
                <div className="tool-bar-right">
                    <Link to="/new-ad">
                        <button class="btn-primary icon-btn">
                            <FeatherIcon icon="flag" />
                            <span>Create Ad</span>
                        </button>
                    </Link>
                </div>
            </div>

        );
    }
}

export default MyAdsToolbar;