import React, { Component } from 'react';
import Tippy from '@tippy.js/react';
import 'tippy.js/dist/tippy.css';
import FeatherIcon from 'feather-icons-react';

class MyAdsTable extends Component {
    render() {
        return (
            <div className="panel w-100">
                <table id="campaigns-table">
                    <thead>
                        <tr>
                            <th className="campaigns-table-name">
                                <label className="checkbox" data-tippy="Select All">
                                    <input type="checkbox" />
                                    <span className="checkmark" />
                                </label>
                                <span>Name</span>
                                <div className="sort" />
                            </th>
                            <th className="campaigns-table-dates">
                                <span>Dates</span>
                                <div className="sort" />
                            </th>
                            <th className="campaigns-table-platforms">
                                <span>Platforms</span>
                                <div className="sort" />
                            </th>
                            <th className="campaigns-table-budget">
                                <span>Total Budget</span>
                                <div className="sort" />
                            </th>
                            <th className="campaigns-table-spent">
                                <span>Spent</span>
                                <div className="sort" />
                            </th>
                            <th className="campaigns-table-clicks">
                                <span>Clicks</span>
                                <div className="sort" />
                            </th>
                            <th className="campaigns-table-cpc">
                                <span>CPC</span>
                                <div className="sort" />
                            </th>
                            <th className="campaigns-table-status">
                                <span>Status</span>
                                <div className="sort" />
                            </th>
                            <th className="campaigns-table-created-by hide">
                                <span>Created By</span>
                                <div className="sort" />
                            </th>
                            <th className="campaigns-table-actions" data-sortable="false" />
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td className="campaigns-table-name">
                                <div className="flex ai-center">
                                    <label className="checkbox">
                                        <input type="checkbox" />
                                        <span className="checkmark" />
                                    </label>
                                    <div className="flex ai-center">
                                        {/* <div class="status error m-r-10"></div> */}
                                        <span>Boost Mangum's Hom</span>
                                    </div>
                                </div>
                            </td>
                            <td className="campaigns-table-dates">
                                <Tippy content={<span>Days remaining: 3</span>}>
                                    <span>12/12/18 - 12/15/18</span>
                                </Tippy>
                            </td>
                            <td className="campaigns-table-total-listings">
                                <div className="platform-icons">
                                    <Tippy content={<span>Facebook</span>}>
                                        <FeatherIcon icon={"facebook"} />
                                    </Tippy>
                                    <Tippy content={<span>Instagram</span>}>
                                        <FeatherIcon icon="instagram" />
                                    </Tippy>
                                </div>
                            </td>
                            <td className="campaigns-table-budget">
                                <Tippy content={<span>Daily Budget: $33.33</span>}>
                                    <span>$200.00</span>
                                </Tippy>
                            </td>
                            <td className="campaigns-table-spent">
                                <span>$100.28</span>
                            </td>
                            <td className="campaigns-table-clicks">
                                <span>13</span>
                            </td>
                            <td className="campaigns-table-cpc">
                                <span>$2.31</span>
                            </td>
                            <td className="campaigns-table-status">
                                <div className="ad-status flex ai-center">
                                    <div className="status error m-r-10" />
                                    <span className="status-label">Error</span>
                                </div>
                            </td>
                            <td className="campaigns-table-created-by hide">
                                <div className="flex ai-center">
                                    <div className="table-avatar m-r-15" style ={ { backgroundImage: "url("+require('../../../assets/images/placeholder/profile-img.png')+")" } } />
                                    <span>DC</span>
                                </div>
                            </td>
                            <td className="campaigns-table-delete actions">
                                <Tippy content={<span>Preview Ad</span>}>
                                    <FeatherIcon icon="eye" />
                                </Tippy>
                                <Tippy content={<span>Edit Ad</span>}>
                                    <FeatherIcon icon="edit-3" />
                                </Tippy>
                            </td>
                        </tr>
                        <tr>
                            <td className="campaigns-table-name">
                                <div className="flex ai-center">
                                    <label className="checkbox">
                                        <input type="checkbox" />
                                        <span className="checkmark" />
                                    </label>
                                    <div className="flex ai-center">
                                        {/* <div class="status running m-r-10"></div> */}
                                        <span>Boost Mangum's Home</span>
                                    </div>
                                </div>
                            </td>
                            <td className="campaigns-table-dates">
                                <Tippy content={<span>Days remaining: 3</span>}>
                                    <span>12/12/18 - 12/15/18</span>
                                </Tippy>
                            </td>
                            <td className="campaigns-table-total-listings">
                                <div className="platform-icons">
                                    <Tippy content={<span>Facebook</span>}>
                                        <FeatherIcon icon={"facebook"} />
                                    </Tippy>
                                    <Tippy content={<span>Instagram</span>}>
                                        <FeatherIcon icon="instagram" />
                                    </Tippy>
                                </div>
                            </td>
                            <td className="campaigns-table-budget">
                                <Tippy content={<span>Daily Budget: $33.33</span>}>
                                    <span>$200.00</span>
                                </Tippy>
                            </td>
                            <td className="campaigns-table-spent">
                                <span>$100.28</span>
                            </td>
                            <td className="campaigns-table-clicks">
                                <span>13</span>
                            </td>
                            <td className="campaigns-table-cpc">
                                <span>$2.31</span>
                            </td>
                            <td className="campaigns-table-status">
                                <div className="ad-status flex ai-center">
                                    <div className="status running m-r-10" />
                                    <span className="status-label">Running</span>
                                </div>
                            </td>
                            <td className="campaigns-table-created-by hide">
                                <div className="flex ai-center">
                                    <div className="table-avatar m-r-15" style ={ { backgroundImage: "url("+require('../../../assets/images/placeholder/profile-img.png')+")" } } />
                                    <span>DC</span>
                                </div>
                            </td>
                            <td className="campaigns-table-delete actions">
                                <Tippy content={<span>Preview Ad</span>}>
                                    <FeatherIcon icon="eye" />
                                </Tippy>
                                <Tippy content={<span>Edit Ad</span>}>
                                    <FeatherIcon icon="edit-3" />
                                </Tippy>
                            </td>
                        </tr>
                        <tr>
                            <td className="campaigns-table-name">
                                <div className="flex ai-center">
                                    <label className="checkbox">
                                        <input type="checkbox" />
                                        <span className="checkmark" />
                                    </label>
                                    <div className="flex ai-center">
                                        {/* <div class="status paused m-r-10"></div> */}
                                        <span>Boost Mangum's Home</span>
                                    </div>
                                </div>
                            </td>
                            <td className="campaigns-table-dates">
                                <Tippy content={<span>Days remaining: 3</span>}>
                                    <span>12/12/18 - 12/15/18</span>
                                </Tippy>
                            </td>
                            <td className="campaigns-table-total-listings">
                                <div className="platform-icons">
                                    <Tippy content={<span>Facebook</span>}>
                                        <FeatherIcon icon={"facebook"} />
                                    </Tippy>
                                    <Tippy content={<span>Instagram</span>}>
                                        <FeatherIcon icon="instagram" />
                                    </Tippy>
                                </div>
                            </td>
                            <td className="campaigns-table-budget">
                                <Tippy content={<span>Daily Budget: $33.33</span>}>
                                    <span>$200.00</span>
                                </Tippy>
                            </td>
                            <td className="campaigns-table-spent">
                                <span>$100.28</span>
                            </td>
                            <td className="campaigns-table-clicks">
                                <span>13</span>
                            </td>
                            <td className="campaigns-table-cpc">
                                <span>$2.31</span>
                            </td>
                            <td className="campaigns-table-status">
                                <div className="ad-status flex ai-center">
                                    <div className="status paused m-r-10" />
                                    <span className="status-label">Paused</span>
                                </div>
                            </td>
                            <td className="campaigns-table-created-by hide">
                                <div className="flex ai-center">
                                    <div className="table-avatar m-r-15" style ={ { backgroundImage: "url("+require('../../../assets/images/placeholder/profile-img.png')+")" } } />
                                    <span>DC</span>
                                </div>
                            </td>
                            <td className="campaigns-table-delete actions">
                                <Tippy content={<span>Preview Ad</span>}>
                                    <FeatherIcon icon="eye" />
                                </Tippy>
                                <Tippy content={<span>Edit Ad</span>}>
                                    <FeatherIcon icon="edit-3" />
                                </Tippy>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>

        );
    }
}

export default MyAdsTable;